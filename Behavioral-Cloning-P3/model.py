import os
import csv
import numpy as np

#import data.py ## This creates the driving_log_balanced.csv file

samples = []
with open('./my_data/driving_log_balanced.csv') as csvfile:
    reader = csv.reader(csvfile)
    next(reader) ## skip the headers
    for line in reader:
        samples.append(line)

# augment the samples by adding left and right camera images and add an indicator for those we want to be flipped
aug_samples = []
correction = 0.25 # how much steering correction to apply to the left/right images
for s in samples:
    aug_samples.append( [ s[0],float(s[3]),0 ] ) #center image, non-flipped
    aug_samples.append( [ s[0],float(s[3]),1 ] ) #center image, flipped
    aug_samples.append( [ s[1],float(s[3])+correction,0 ] ) #left image image, non-flipped
    aug_samples.append( [ s[2],float(s[3])-correction,0 ] ) #center image, non-flipped

    aug_samples.append( [ s[1],float(s[3])+correction,1 ] ) #left image image, flipped
    aug_samples.append( [ s[2],float(s[3])-correction,1 ] ) #center image, flipped


def flip( image , measurement ):
    ''' Horizontally flip an image and its associated steering angle'''
    image = cv2.flip( image, 1 )
    measurement = measurement * -1.0
    return image,measurement


from sklearn.model_selection import train_test_split
train_samples, validation_samples = train_test_split(aug_samples, test_size=0.2)

import cv2
import sklearn
from random import shuffle

def preprocess( sample ):
    # get the image
    name = './my_data/IMG/'+sample[0].split('/')[-1]
    image = cv2.imread(name)
    
    ## inject random brightness so it's not a factor
    cv2.cvtColor(image,cv2.COLOR_BGR2YUV) ## change color space
    image = image.astype(np.float64)
    image[:,:,0] *= 1.0 + np.random.uniform( -1 , 1 ) * 0.5
    image[:,:,0] = np.clip( image[:,:,0] , 0. , 255. )
    image = image.astype(np.uint8)
    
    ## change color space back to RGB as in the drive.py file
    cv2.cvtColor(image,cv2.COLOR_YUV2RGB) 
    
    # get the steering angle
    angle = sample[1]

    # flip if required
    if sample[2] == 1:
        image,angle = flip( image, angle )

    return image,angle

def generator(samples, batch_size=32):
    num_samples = len(samples)
    while 1: # Loop forever so the generator never terminates
        shuffle(samples)
        for offset in range(0, num_samples, batch_size):
            batch_samples = samples[offset:offset+batch_size]

            images = []
            angles = []
            for batch_sample in batch_samples:
                image, angle = preprocess ( batch_sample )
                images.append(image)
                angles.append(angle)

            X_train = np.array(images)
            y_train = np.array(angles)

            yield sklearn.utils.shuffle(X_train, y_train)

# Set our batch size
batch_size=32

# compile and train the model using the generator function
train_generator = generator(train_samples, batch_size=batch_size)
validation_generator = generator(validation_samples, batch_size=batch_size)

from keras.models import Sequential
from keras.layers import Flatten, Dense, Lambda, Dropout
from keras.layers.convolutional import Conv2D
from keras.layers.pooling import MaxPooling2D
from keras.callbacks import ModelCheckpoint
from keras.layers import Cropping2D


## Model Architecture
model = Sequential()

## Preprocessing
model.add ( Cropping2D ( cropping=((50,20), (0,0)), input_shape=(160,320,3) ))
model.add ( Lambda( lambda x: x/127.5 - 1. ))

## Convolutional 
model.add( Conv2D(16, (3, 3), activation='relu'))
model.add( MaxPooling2D(pool_size=(2, 2)))
model.add( Conv2D(32, (3, 3), activation='relu'))
model.add( MaxPooling2D(pool_size=(2, 2)))
model.add( Conv2D(64, (3, 3), activation='relu'))
model.add( MaxPooling2D(pool_size=(2, 2)))

## Fully Connected
model.add( Flatten())
model.add( Dense(500, activation='relu'))
model.add( Dropout(rate=.5))
model.add( Dense(100, activation='relu'))
model.add( Dropout(rate=.25))
model.add( Dense(20, activation='relu'))
model.add( Dense(1))

#  Compile the model
model.compile ( loss = 'mse', optimizer = 'adam' )

from keras.models import Model
import matplotlib.pyplot as plt

# use this to save the model periodically so that we don't waste tons of GPU time for nothing
checkpoint = ModelCheckpoint('model-{epoch:03d}.h5',
                             monitor='val_loss',
                             verbose=1,
                             save_best_only=True,
                             mode='auto')

from workspace_utils import active_session
 
with active_session():
    history_object = model.fit_generator(train_generator, \
        samples_per_epoch = len(train_samples), validation_data = validation_generator, \
        nb_val_samples = len(validation_samples), nb_epoch=5, verbose=1, \
        callbacks=[checkpoint])
    # When finished save the model
    model.save ( 'model.h5' )
    