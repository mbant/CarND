import csv
import pandas as pd
import numpy as np

# read the original csv file
df = pd.io.parsers.read_csv('./my_data/driving_log.csv',header=[-1])

df_new = pd.DataFrame()   # Create the new dataset
bins = 1000               # N of bins
bin_n = 300               # N of examples to include in each bin (at most)

## for all bins, sample randomply up to bin_n images that have the steering angle between its limits
low = 0
for high in np.linspace(0, 1, num=bins):  
    found = df[(np.absolute(df[3]) >= low) & (np.absolute(df[3]) < high)]
    n_sample = min(bin_n, found.shape[0])
    df_new = pd.concat([df_new, found.sample(n_sample)])
    low = high # go to next bin

# save this new balanced csv
df_new.to_csv('./my_data/driving_log_balanced.csv', index=False)

