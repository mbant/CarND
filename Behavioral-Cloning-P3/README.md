# Behavioral Cloning Project

[![Udacity - Self-Driving Car NanoDegree](https://s3.amazonaws.com/udacity-sdc/github/shield-carnd.svg)](http://www.udacity.com/drive)

Overview
---

In this project, I applied my knowledge about deep neural networks and convolutional neural networks to clone driving behavior (on the [Udacity Simulator](https://github.com/udacity/self-driving-car-sim)) using Keras with Tensorflow backend. 

The model will output a steering angle to an autonomous vehicle with deterministic throttle based on a desired constant speed.

The Project
---

The goals / steps of this project are the following:
* Use the simulator to collect data of good driving behavior 
* Augment the data to increase the NN performances
* Design, train and validate a model that predicts a steering angle from image data
* Use the model to drive the vehicle autonomously around the first track in the simulator. 

Data Augmentation and Preprocessing
---

In order to improve our Deep Neural Netwrok performances we want to make the most out of the training data we collected by recording our driving on the simulator (on both tracks).
To do that we apply a few simple tricks:

* left and right camera images can be used by just applying a small correction of `0.25`  to the steering angles;
* images can be flipped horizontally to obtain data for essentially a new track; we simply invert the sign of the steering angle and easily double our sample size this way.
    Note moreover that this as well as driving the simulator on the track in both directions helps alleviating the bias toward the left that the trained model would have if we were to drive in one direction only on the first track.
* finally a simple random brightness injection can be applied to help the network generalise over different light conditions; this is part of a change in colorspace which is anyway necessary to match the BGR colorspace used to read the images in the generator and the RGB colorspace used by the `drive.py` file when driving in autonomous mode.

By observing the distribution of the steering angles though we can see that most of them are very close to zero; understandable since most of the time we're driving straight, at least on the first track. While we might want to preserve a bias toward going straight to avoid the car just wobbling left and right on the track, this much is probably more than is necessary.

![hist1](images/skewed.png)

I decided thus to bin the measurements distributions and to randomly subsample within each bin a maximum number of images ( the actual number depends on bin size, see `data.py` for the code )
The result is this much more balanced dataset of `10332` images ( from `37734` ), that will be the one fed to the above preprocessing steps to obtain the final database which will be split 80/20 between training and validation.

![hist2](images/balanced.png)


Finally, before feeding each image to the Deep NN, I crop the top 50 pixels and the bottom 20 via the `Cropping2D ( cropping=((50,20), (0,0)) )` Keras layer ( to erase useless information about the environment and the trunk of the car ); a more aggressive crop might work, but the second track is hilly and thus the road appears at times ina wider area with respect to the first track, I found these values to be suitable.

Finally each image is normalised via a simple `Lambda` layer using `lambda x: x/127.5 - 1.` so that each pixel takes value between -1 and 1.


Network Architecture
---

I started using the suggested [NVIDIA](https://devblogs.nvidia.com/deep-learning-self-driving-cars/) architecture, adapted to fit our input dimensions, but quickly realised that because we're using a simulator we find ourselves ina  much less complex environment/setting.
I thus iteratively simplified my network up till the final design shown below:


| Layer                		|     Description	        					| 
|:-------------------------:|:---------------------------------------------:| 
| Input             		| 90x320x3 RGB image (after cropping)           | 
| **16** 3x3 Conv filters   | 1x1 stride, valid padding                  	|
| RELU				    	|												|
| Max pooling	      	    | 2x2 stride                    				|
| **32** 3x3 Conv filters   | 1x1 stride, valid padding                  	|
| RELU				    	|												|
| Max pooling	        	| 2x2 stride                    				|
| **64** 3x3 Conv filters   | 1x1 stride, valid padding                  	|
| RELU				    	|												|
| Max pooling	        	| 2x2 stride                    				|
| Fully Connected		    | 500 neurons              						|
| RELU					    |				                            	|
| Droupout                  | rate = 0.5 ( defined as 1-keep prob)          |
| Fully connected		    | output 100            						|
| RELU					    |				                            	|
| Droupout                  | rate = 0.25 or  keep_prob = 0.75              |
| Fully connected		    | output 20            						    |
| RELU					    |				                            	|
| Fully connected		    | 1 output  **predicted steering angle**        | 
 
This network is significantly lighter than theproposed NVIDIA architecture but still sufficiently accurate for our purposes.


Output
---

The car results a bit wobbly and probably could use some more trainig data. The cut to most of the "zero angle" frames as well as the full "recovery lap" used for training on both tracks skewed the Deep Neural Network to `think` <img src="images/robot-face.png" alt="drawing" width="20"/> that the wheel needs to be constantly moved. I'll gladly have another go at this as soon as I can to correct for this problem.

Here's the front facing camera view of the car in autonomous mode, as captured in the project video:

![gif-autonomous-2](images/autonomous2.gif)

![gif-autonomous](images/autonomous.gif)

<br/><br/>

<br/><br/>

<br/><br/>

<br/><br/>

## Notes on the code in this repo

### Dependencies
To run the code the following items are required:

* [CarND Term1 Starter Kit](https://github.com/udacity/CarND-Term1-Starter-Kit)

The lab enviroment can be created with CarND Term1 Starter Kit. Click [here](https://github.com/udacity/CarND-Term1-Starter-Kit/blob/master/README.md) for the details.

The following resources can be found in this github repository:
* data.py
* drive.py
* video.py
* README.md  (the functions as project writeup as well)

The simulator can be downloaded from the [the github repo](https://github.com/udacity/self-driving-car-sim).

## Details About Files In This Directory

### `drive.py`

Usage of `drive.py` requires you have the trained model as an h5 file, i.e. `model.h5`.


```sh
python drive.py model.h5
```

The above command will load the trained model and use the model to make predictions on individual images in real-time and send the predicted angle back to the server via a websocket connection.

Note: There is known local system's setting issue with replacing "," with "." when using drive.py. When this happens it can make predicted steering values clipped to max/min values. If this occurs, a known fix for this is to add "export LANG=en_US.utf8" to the bashrc file.

#### Saving a video of the autonomous agent

```sh
python drive.py model.h5 P3_video
```

The fourth argument, `P3_video`, is the directory in which to save the images seen by the agent. If the directory already exists, it'll be overwritten.
Each image file name is a timestamp of when the image was seen. This information is used by `video.py` to create a chronological video of the agent driving.

### `video.py`

```sh
python video.py P3_video
```

Creates a video based on images found in the `P3_video` directory. The name of the video will be the name of the directory followed by `'.mp4'`, so, in this case the video will be `P3_video.mp4`.

Optionally, one can specify the FPS (frames per second) of the video:

```sh
python video.py P3_video --fps 48
```

Will run the video at 48 FPS. The default FPS is 60.