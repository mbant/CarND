#include "PID.h"
#include "json.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <uWS/uWS.h>

// for convenience
using nlohmann::json;
using std::string;

// For converting back and forth between radians and degrees.
constexpr double pi() { return M_PI; }
double deg2rad(double x) { return x * pi() / 180; }
double rad2deg(double x) { return x * 180 / pi(); }

// Checks if the SocketIO event has JSON data.
// If there is data the JSON object in string format will be returned,
// else the empty string "" will be returned.
string hasData(string s) {
  auto found_null = s.find("null");
  auto b1 = s.find_first_of("[");
  auto b2 = s.find_last_of("]");
  if (found_null != string::npos) {
    return "";
  } else if (b1 != string::npos && b2 != string::npos) {
    return s.substr(b1, b2 - b1 + 1);
  }
  return "";
}

int main() {
  uWS::Hub h;

  PID steer_pid;
  // Init at the values given by the PID final lecture as initial guess
  steer_pid.Init(0.12, 0.0025, 1.);
  // best guess by hand 0.1, 0.005, 0.75
  // best found through TW/SGD 0.12, 0.0026, 1.05

  S_PID speed_pid;
  speed_pid.Init(.2, 0., .5);

#if 0
  steer_pid.ToggleTWIDDLE(); // toggle twiddle ON, this disable SGD as well
  speed_pid.ToggleSGD(); // toggle optim off
#else
  steer_pid.ToggleSGD(); // toggle optim off without turnin twiddle on
  speed_pid.ToggleSGD(); // toggle optim off
#endif

  h.onMessage([&steer_pid, &speed_pid](uWS::WebSocket<uWS::SERVER> ws,
                                       char *data, size_t length,
                                       uWS::OpCode opCode) {
    steer_pid.setWebSocket(ws);

    // "42" at the start of the message means there's a websocket message event.
    // The 4 signifies a websocket message
    // The 2 signifies a websocket event
    if (length && length > 2 && data[0] == '4' && data[1] == '2') {
      auto s = hasData(string(data).substr(0, length));

      if (s != "") {
        auto j = json::parse(s);

        string event = j[0].get<string>();

        if (event == "telemetry") {
          // j[1] is the data JSON object
          double angle = std::stod(j[1]["steering_angle"].get<string>());
          double cte = std::stod(j[1]["cte"].get<string>());

          std::cout << "STEER PID: ";
          double steer_value = steer_pid.GetControl(cte);

          /**
           * NOTE: Feel free to play around with the throttle and speed.
           *   Maybe use another PID controller to control the speed!
           */
          double speed = std::stod(j[1]["speed"].get<string>());
          // desired speed: mph -- target MORE on straight road, LESS on turns
          double desired_speed = 10. * (2.0 - 2 * abs(steer_value)) + 20;
          // this incentivise to stay UNDER the desired speed and
          // penalises more higher velocities
          double speed_error = speed - desired_speed;
          // speed_error = speed_error>0?speed_error*speed_error:-speed_error;

          std::cout << "SPEED PID: ";
          double throttle = speed_pid.GetControl(speed_error);
          // double throttle = 0.3;

          // DEBUG
          std::cout << "CTE: " << cte << " Steering Value: " << steer_value
                    << std::endl
                    << "Speed: " << speed << " (/" << desired_speed
                    << ") Throttle: " << throttle << std::endl
                    << std::endl;

          json msgJson;
          msgJson["steering_angle"] = steer_value;
          msgJson["throttle"] = throttle;
          auto msg = "42[\"steer\"," + msgJson.dump() + "]";
          // std::cout << msg << std::endl;
          ws.send(msg.data(), msg.length(), uWS::OpCode::TEXT);
        } // end "telemetry" if
      } else {
        // Manual driving
        string msg = "42[\"manual\",{}]";
        ws.send(msg.data(), msg.length(), uWS::OpCode::TEXT);
      }
    } // end websocket message if
  }); // end h.onMessage

  h.onConnection([&h](uWS::WebSocket<uWS::SERVER> ws, uWS::HttpRequest req) {
    std::cout << "Connected!!!" << std::endl;
  });

  h.onDisconnection([&h](uWS::WebSocket<uWS::SERVER> ws, int code,
                         char *message, size_t length) {
    ws.close();
    std::cout << "Disconnected" << std::endl;
  });

  int port = 4567;
  if (h.listen(port)) {
    std::cout << "Listening to port " << port << std::endl;
  } else {
    std::cerr << "Failed to listen to port" << std::endl;
    return -1;
  }

  h.run();
}