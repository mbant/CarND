#include "PID.h"

PID::PID() {}

PID::~PID() {}

void PID::Init(double Kp_, double Ki_, double Kd_) {
  Kp = Kp_;
  Ki = Ki_;
  Kd = Kd_;

  p_error = 0.;
  i_error = 0.;
  d_error = 0.;
  total_squared_error = 0.;

  dp_p = 0.2 * fabs(Kp);
  dp_i = 0.2 * fabs(Ki);
  dp_d = 0.2 * fabs(Kd);
}

void PID::ToggleSGD() {
  if (sgd) {
    sgd = false;
  } else {
    twiddle = false;
    sgd = true;
  }
}

void PID::ToggleTWIDDLE() {
  if (twiddle) {
    twiddle = false;
  } else {
    sgd = false;
    twiddle = true;
  }
}

void PID::UpdateTotalError() { total_squared_error += p_error * p_error; }

void PID::UpdateError(double e) {
  d_error = e - p_error;
  p_error = e;
  i_error += e;
  // ai_error += (fabs(e)/ - ai_error) / count;
  ai_error += fabs(e);

  UpdateTotalError();
}

void PID::UpdateBatchDerivatives() {
  // formula is mean + (newVal - mean)/count -- but remember that dK* = -*_error
  dKp += (-p_error - dKp) / count;
  dKi += (-i_error - dKi) / count;
  dKd += (-d_error - dKd) / count;
}

double PID::ComputeControl() {
  return -Kp * p_error - Ki * i_error - Kd * d_error;
}

void PID::SGD() {

  UpdateBatchDerivatives();

  if (count == batch_size) {
    // update params
    BackProp();

    // reset derivatives
    // i_error = 0.;
    ai_error = 0.;
    dKp = 0.;
    dKi = 0.;
    dKd = 0.;

    // reset count
    count = 0;
  }
}

std::ostream &operator<<(std::ostream &os, const PID::TwiddleState &ts) {
  switch (ts) {
  case PID::TwiddleState::init:
    return os << "Init" << std::endl;
    break;

  case PID::TwiddleState::up_and_restart:
  case PID::TwiddleState::up_update:
    return os << "Up" << std::endl;
    break;
  case PID::TwiddleState::down_and_restart:
  case PID::TwiddleState::down_update:
    return os << "Down" << std::endl;
    break;
  case PID::TwiddleState::end:
  case PID::TwiddleState::tuned:
    return os << "Tuned" << std::endl;
    break;
  }
}

void PID::NextParam() {
  if (tw_curr_p == TwiddleCurrentParam::P) {
    tw_curr_p = TwiddleCurrentParam::I;
    return;
  } else if (tw_curr_p == TwiddleCurrentParam::I) {
    tw_curr_p = TwiddleCurrentParam::D;
    return;
  } else {
    tw_curr_p = TwiddleCurrentParam::P;
    return;
  }
}

void PID::NextCycle() {

  switch (tw_state) {
  case TwiddleState::up_update:
    // we stay in this parameter but switch to down and re-run the training
    tw_state = TwiddleState::down_and_restart;
    break;

  case TwiddleState::down_update:
    // reset parameter to the starting point
    if (tw_curr_p == TwiddleCurrentParam::P) {
      Kp += dp_p;
    } else if (tw_curr_p == TwiddleCurrentParam::I) {
      Ki += dp_i;
    } else {
      Kd += dp_d;
    }
    // downscale dp
    if (tw_curr_p == TwiddleCurrentParam::P) {
      dp_p *= 0.9;
    } else if (tw_curr_p == TwiddleCurrentParam::I) {
      dp_i *= 0.9;
    } else {
      dp_d *= 0.9;
    }
    // go the next param and cycle
    NextParam();
    tw_state = TwiddleState::up_and_restart;
    break;
  }
}

void PID::Twiddle() {

  std::cout << "Twiddle State: " << tw_state << "  ";

  switch (tw_state) {
  case TwiddleState::init: // run the first time we get here
    if (tw_it < tw_n_iterations) {
      ++tw_it;
      // errors + total_error are updated outside
    } else {
      // now we run through a whole cycle, so we init the error
      tw_best_error =
          total_squared_error / static_cast<double>(tw_n_iterations);
      // and get to the real start
      tw_curr_p = TwiddleCurrentParam::P;
      tw_state = TwiddleState::up_and_restart;
    }
    break;

  case TwiddleState::up_and_restart: // to run, while tw_tollerance > sum(dp)
    if ((dp_p + dp_i + dp_d) <= tw_tollerance) {
      tw_state = TwiddleState::end;
    } else {
      // up and restart
      if (tw_curr_p == TwiddleCurrentParam::P) {
        Kp += dp_p;
      } else if (tw_curr_p == TwiddleCurrentParam::I) {
        Ki += dp_i;
      } else {
        Kd += dp_d;
      }
      Restart();
      tw_state = TwiddleState::up_update;
    }
    break;

  case TwiddleState::down_and_restart: // to run, while tw_tollerance > sum(dp)
    if ((dp_p + dp_i + dp_d) <= tw_tollerance) {
      tw_state = TwiddleState::end;
    } else {
      // down and restart
      if (tw_curr_p == TwiddleCurrentParam::P) {
        Kp -= 2 * dp_p;
      } else if (tw_curr_p == TwiddleCurrentParam::I) {
        Ki -= 2 * dp_i;
      } else {
        Kd -= 2 * dp_d;
      }
      Restart();
      tw_state = TwiddleState::down_update;
    }
    break;

  case TwiddleState::up_update:
    if (tw_it < tw_n_iterations) {

      // errors + total_error are updated outside
      std::cout << "Current Error: "
                << total_squared_error / static_cast<double>(tw_it)
                << " -- Best Error: " << tw_best_error << std::endl;
      ++tw_it;

      if (total_squared_error / static_cast<double>(tw_n_iterations) >=
          tw_best_error) { // no chance of recovery
        std::cout << "No chance of recovery: " << total_squared_error << " "
                  << total_squared_error / static_cast<double>(tw_n_iterations)
                  << " " << tw_best_error << std::endl << std::endl;
        NextCycle();
      }

    } else {
      // now we run through a whole cycle, so we set the error
      tw_error = total_squared_error / static_cast<double>(tw_n_iterations);

      if (tw_error < tw_best_error) {
        // update best error
        tw_best_error = tw_error;

        // upscale dp
        if (tw_curr_p == TwiddleCurrentParam::P) {
          dp_p *= 1.1;
        } else if (tw_curr_p == TwiddleCurrentParam::I) {
          dp_i *= 1.1;
        } else {
          dp_d *= 1.1;
        }
        // go to the next param
        NextParam();
        // and to the next cycle
        tw_state = TwiddleState::up_and_restart;
      } else {
        NextCycle();
      }
    }
    break;

  case TwiddleState::down_update:
    if (tw_it < tw_n_iterations) {

      // errors + total_error are updated outside
      std::cout << "Current Error: "
                << total_squared_error / static_cast<double>(tw_it)
                << " -- Best Error: " << tw_best_error << std::endl;
      ++tw_it;

      if (total_squared_error / static_cast<double>(tw_n_iterations) >=
          tw_best_error) { // no chance of recovery
        std::cout << "No chance of recovery: " << total_squared_error << " "
                  << total_squared_error / static_cast<double>(tw_n_iterations)
                  << " " << tw_best_error << std::endl << std::endl;
        NextCycle();
      }

    } else {
      // now we run through a whole cycle, so we set the error
      tw_error = total_squared_error / static_cast<double>(tw_n_iterations);

      if (tw_error < tw_best_error) {
        // update best error
        tw_best_error = tw_error;

        // upscale dp
        if (tw_curr_p == TwiddleCurrentParam::P) {
          dp_p *= 1.1;
        } else if (tw_curr_p == TwiddleCurrentParam::I) {
          dp_i *= 1.1;
        } else {
          dp_d *= 1.1;
        }
        // go to the next param
        NextParam();
        // and to the next cycle
        tw_state = TwiddleState::up_and_restart;
      } else {
        NextCycle();
      }
    }
    break;

  case TwiddleState::end: // we're finished once we reach here
    Restart();            // restart the simulator with final coefficients
    ToggleTWIDDLE();      // never look back
    break;
  }
}

double PID::GetControl(double e) {
  UpdateError(e);

  if (sgd) {
    SGD();
  } else if (twiddle) {
    Twiddle();
  }

  std::cout << "Kp: " << Kp << " Ki: " << Ki << " Kd: " << Kd << std::endl;

  // Compute control and update count
  double control = ComputeControl();
  ++count;
  ++uber_count;

  return std::min(1., std::max(-1., control));
}

double PID::GetTotalError() { return total_squared_error; }

void PID::BackProp() {
  // Adapt learning rates with RMSProp
  double gamma = 0.8;
  if (lr_p > 0.) {
    lr_p = lr_p * gamma + (1. - gamma) * (dKp * dKp);
    lr_i = lr_i * gamma + (1. - gamma) * (dKi * dKi);
    lr_d = lr_d * gamma + (1. - gamma) * (dKd * dKd);
  } else {
    lr_p = (dKp * dKp);
    lr_i = (dKi * dKi);
    lr_d = (dKd * dKd);
  }

  // Update Parameters
  double base_lr = 0.05;
  Kp -= base_lr / sqrt(lr_p) * (dKp);
  Ki -= base_lr / sqrt(lr_i) * (dKi);
  Kd -= base_lr / sqrt(lr_d) * (dKd);
}

// S_PID has (by default) a 0. Ki component and a modified derivative for Ks
S_PID::S_PID() : PID::PID() { Ki = 0.; } // by default

void S_PID::BackProp() {
  // Adapt learning rates with RMSProp
  double gamma = 0.75;
  lr_p = lr_p * gamma + (1. - gamma) * (dKp * dKp);
  lr_i = lr_i * gamma + (1. - gamma) * (dKi * dKi);
  lr_d = lr_d * gamma + (1. - gamma) * (dKd * dKd);

  // Update Parameters
  double base_lr = 0.01;
  Kp -= base_lr / sqrt(lr_p) * (-dKp);
  Kd -= base_lr / sqrt(lr_d) * (dKd);
  // Ki unchanged
}

void PID::Restart() {

  // reset variables
  p_error = 0;
  i_error = 0;
  d_error = 0;
  total_squared_error = 0.;

  tw_it = 0;
  tw_error = std::numeric_limits<double>::max();

  // restart the simulator
  std::string reset_msg = "42[\"reset\",{}]";
  ws.send(reset_msg.data(), reset_msg.length(), uWS::OpCode::TEXT);
}
