#include <algorithm>
#include <cmath>
#include <iostream>

#include <uWS/uWS.h>

#ifndef PID_H
#define PID_H

class PID {
public:
  /**
   * Constructor
   */
  PID();

  /**
   * Destructor.
   */
  virtual ~PID();

  /**
   * Initialize PID.
   * @param (Kp_, Ki_, Kd_) The initial PID coefficients
   */
  void Init(double Kp_, double Ki_, double Kd_);

  /**
   * Get the desired control, predicted given a new error.
   * @param e The current error
   * @output a normalised control between [-1,1]
   */
  double GetControl(double e);
  void setWebSocket(uWS::WebSocket<uWS::SERVER> ws_) { ws = ws_; }

  /**
   * Return the total PID error.
   * @output The total PID error
   */
  double GetTotalError();

  void ToggleSGD();
  void ToggleTWIDDLE();

  void SetTwiddleTollerance(double t) { tw_tollerance = t; }
  double GetTwiddleTollerance() { return tw_tollerance; }

protected:
  /**
   * Update the PID error variables given error.
   * @param e The current error
   */
  void UpdateError(double e);
  /**
   * Update the total PID error.
   */
  void UpdateTotalError();
  /*
   * Update the derivatives used in SGD
   */
  void UpdateBatchDerivatives();

  /**
   * PID Errors
   */
  double p_error{0.};
  double i_error{0.};
  double ai_error{0.};
  double d_error{0.};

  // Total Error
  double total_squared_error{0.};
  double previous_tse{0.};

  /**
   * PID Coefficients
   */
  double Kp;
  double Ki;
  double Kd;

  double ComputeControl();

  /*
   * Parameter optimisation via SGD ( with RMSProp? )
   * partial derivatives with respect to Kp, Ki and Kd of the control function
   * f(Kp, Ki, Kd) are equal to -p_error, -i_error and -d_error accordingly
   */
  bool sgd{true};
  void SGD();
  virtual void BackProp();

  // learning rates
  // double lr_p{1.};
  // double lr_i{10.};
  // double lr_d{0.0005};
  double lr_p{-1};
  double lr_i{-1};
  double lr_d{-1};

  // batch derivatives
  double dKp{0.};
  double dKi{0.};
  double dKd{0.};

  // bath size
  size_t count{1};
  size_t uber_count{1};
  size_t batch_size{200};

  /*
   * Parameter optimisation via Twiddle / Coordinate Ascent
   */
  bool twiddle{false};
  void Twiddle();

  double tw_best_error{std::numeric_limits<double>::max()};
  double tw_error{std::numeric_limits<double>::max()};
  double tw_tollerance{0.1};

  size_t tw_n_iterations{500}; // total iterations
  size_t tw_it{0}; // current iteration

  double dp_p;
  double dp_i;
  double dp_d;
public:
  enum class TwiddleState { init, up_and_restart, down_and_restart, up_update, down_update, end, tuned };
protected:
  TwiddleState tw_state{TwiddleState::init};


  enum class TwiddleCurrentParam { P, I, D };
  TwiddleCurrentParam tw_curr_p{TwiddleCurrentParam::P};
  void NextParam();
  void NextCycle();

  /*
   * Restart the simulator
   */
  void Restart();
  uWS::WebSocket<uWS::SERVER> ws; // This is a copy, but ws is "only a pointer"

};

class S_PID : public PID {
public:
  S_PID();

private:
  void BackProp();
};

#endif // PID_H