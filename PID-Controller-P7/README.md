# CarND-Controls-PID
Self-Driving Car Engineer Nanodegree Program

---

This is a PID (proportional-integrated-derivative) controller used to maneuver a self-driving (simulated) vehicle around a lake track; the car has a pre-coded path through the track to follow and the aim is to design a system that track it through the use of throttle and steering alone via PID controllers.

The simulator provides cross track error (CTE) and the speed (mph) of the car at regular intervals and this data is used to compute the appropriate steering angle and throttle to apply.

The PID itself is quite easy to design as it is only necessary to compute, given the current error `e`, the instantaneous error, the integrated error from the beginning of the experiment and the derivative of the error at this time; we are further simplifying the problem by discretising the time dimension, which leaves us with the following update

    d_error = e - p_error;
    p_error = e;
    i_error += e;

for the `d` derivative error, `p` instantaneous and `i` integrated errors respectively. The output control is then easily computed as

    -Kp * p_error - Ki * i_error - Kd * d_error;

Let's take the PID used for steering for example:

The P, or “proportional”, component has the most direct effect on the car’s behavior as it controls the steering angle inversely proportionally to the car’s distance from the path. Bigger values of `Kp` result in larger steering angles with respect to the CTE.

The D, or “differential”, component counteracts the tendency of a simpler P-Controller (one with only a P component) to overshoot the path and periodically oscillate around it. I observed that not only `Kd` usually needs to grow along with `Kp`, but also is usually neede to be larger in magnitude.

The I, or “integral”, component finally addresses systematic biases in the CTE which would prevents a PD-Controller from reaching the center line. This bias can take several forms, *e.g.* as a steering drift, but I suspect in this experiment at least a small `Ki` will be necessary regardless since the track is a loop with mostly left turns and thus our car will need to steer left more often than right.

Note that the throttle is controlled via a PID as well, and I chose as a measure of error the difference between the current speed and a target speed that increases when the current steering angle is small and *vice-versa*, as it's probably safer to slow down while turning (no snow on this track!).

## Parameter Tuning

The harder part of the project is the tuning of the PID parameters for both the controllers. I first run a few experiments (essentially a grid-search) to get an idea of a reasonable parameter space.

From those starting points I then optimised `(Kp, Ki, Kd)` via SGD for the throttle controller and via Twiddle, a sort of Coordinate Ascend algorithm introduced in the class, for the steering controller.

A minibatch version of SGD (with RMSProp schedule for the learning rate, as the three parameters and errors have VASTLY different ranges) was trivial to implement since the output function is linear in the parameters.
Twiddle is also conceptually easy but I decided to challenge myself and made use of the [uWebSockets](!https://github.com/uWebSockets/uWebSockets) library used in the simulator to communicate (via JSON messages) with the `cpp` code and designed a function that run twiddle for a certain horizon/epoch before **restarting** completely untill convergence is reached; this required a little effort to maintain the correct status inside the `PID` object and I had to code the algorithm as a sort-of finite state machine to keep track of which action to take. Fun! See the code between lines `149-293` in `PID.cpp` and a few helper functions just before ( plus various definitions around `110-135` of `PID.h`).

At the very beginning, before the grid search, thigs were looking rough ..
<p  style="text-align:center"><img src="bad_start_small.png" alt="drawing" width="400"/></p>
.. after a while SGD and twiddle started making a difference but the car was still oscillating a lot ..
<p  style="text-align:center"><img src="twiddle_1_small.gif" alt="drawing" width="400"/></p>
.. untill finally some parameters started to make more sense and the vehicle was able to complete multiple laps without going off-track of performing dangerous turns!
<p  style="text-align:center"><img src="twiddle_better_small.gif" alt="drawing" width="400"/></p>

Note than in particular the Twiddle algorithm is very time consuming as it re-run the simulation over and over untill convergence, so I've disabled it by default and I initialise with the parameters that were deemed optimal in a previous run.
To run it again change line `44` in `main.cpp` to `#if 1`.

## Basic Build Instructions

1. Clone this repo.
2. Make a build directory: `mkdir build && cd build`
3. Compile: `cmake .. && make`
4. Run it: `./pid`. 

### Dependencies

* cmake >= 3.5
 * All OSes: [click here for installation instructions](https://cmake.org/install/)
* make >= 4.1(mac, linux), 3.81(Windows)
  * Linux: make is installed by default on most Linux distros
  * Mac: [install Xcode command line tools to get make](https://developer.apple.com/xcode/features/)
  * Windows: [Click here for installation instructions](http://gnuwin32.sourceforge.net/packages/make.htm)
* gcc/g++ >= 5.4
  * Linux: gcc / g++ is installed by default on most Linux distros
  * Mac: same deal as make - [install Xcode command line tools]((https://developer.apple.com/xcode/features/)
  * Windows: recommend using [MinGW](http://www.mingw.org/)
* [uWebSockets](https://github.com/uWebSockets/uWebSockets)
  * Run either `./install-mac.sh` or `./install-ubuntu.sh`.
  * If you install from source, checkout to commit `e94b6e1`, i.e.
    ```
    git clone https://github.com/uWebSockets/uWebSockets 
    cd uWebSockets
    git checkout e94b6e1
    ```
    Some function signatures have changed in v0.14.x. See [this PR](https://github.com/udacity/CarND-MPC-Project/pull/3) for more details.
* Simulator. You can download these from the [project intro page](https://github.com/udacity/self-driving-car-sim/releases) in the classroom.