#include "ukf.h"
#include "Eigen/Dense"

using Eigen::MatrixXd;
using Eigen::VectorXd;

/**
 * Initializes Unscented Kalman filter
 */
UKF::UKF() {
  // if this is false, laser measurements will be ignored (except during init)
  use_laser_ = true;

  // if this is false, radar measurements will be ignored (except during init)
  use_radar_ = true;

  // state vector dimension
  n_x_ = 5;

  // initial state vector 
  // this will be initialised with the first measurement
  x_ = VectorXd(n_x_);

  // initial covariance matrix
  P_ = MatrixXd(n_x_, n_x_);
  P_ << 10, 0, 0, 0, 0,
        0, 10, 0, 0, 0,
        0, 0, 10, 0, 0,
        0, 0, 0, 10, 0,
        0, 0, 0, 0, 1;

  // Process noise standard deviation longitudinal acceleration in m/s^2
  std_a_ = 1.5;

  // Process noise standard deviation yaw acceleration in rad/s^2
  std_yawdd_ = 0.5;

  /**
   * DO NOT MODIFY measurement noise values below.
   * These are provided by the sensor manufacturer.
   */

  // Laser measurement noise standard deviation position1 in m
  std_laspx_ = 0.15;

  // Laser measurement noise standard deviation position2 in m
  std_laspy_ = 0.15;

  // Radar measurement noise standard deviation radius in m
  std_radr_ = 0.3;

  // Radar measurement noise standard deviation angle in rad
  std_radphi_ = 0.03;

  // Radar measurement noise standard deviation radius change in m/s
  std_radrd_ = 0.3;
  
  /**
   * End DO NOT MODIFY section for measurement noise values 
   */

  // Augmented state dimension
  n_aug_ = n_x_ + 2; 

  // sigma points parameters
  lambda_ = 3 - n_x_;

  // Initialize weights ( these remain constant ! )
  weights_ = VectorXd(2 * n_aug_ + 1);
  weights_.fill(0.5 / (n_aug_ + lambda_));
  weights_(0) = lambda_ / (lambda_ + n_aug_); // first one is "special"

  Xsig_pred_ = MatrixXd(n_x_, 2*n_aug_+1 );

  is_initialized_ = false;

}

UKF::~UKF() {}

double UKF::NormalizeAngle(double angle) {
  /**
   *  Normalized the angle to be inside [-M_PI, M_PI] interval.
   */
  while (angle> M_PI) angle-=2.*M_PI;
  while (angle<-M_PI) angle+=2.*M_PI;
  return angle;
}


void UKF::ProcessMeasurement(MeasurementPackage meas_package) {
  
  /**
   * Process each incoming measurement
   */
  // std::cout << "NEW ";
  // if (meas_package.sensor_type_ == MeasurementPackage::RADAR )
  //   std::cout << " RADAR ";
  // else
  //   std::cout << " LASER ";
  // std::cout << "MEASUREMENT ARRIVED ..." << std::flush;

  // If the UKF is not initialised (i.e. first measurement)
  if ( !is_initialized_) {
    if (meas_package.sensor_type_ == MeasurementPackage::RADAR) {
      double rho = meas_package.raw_measurements_[0]; // range
      double phi = meas_package.raw_measurements_[1]; // bearing
      double rho_dot = meas_package.raw_measurements_[2]; // rho change rate
      double px = rho * cos(phi);
      double py = rho * sin(phi);
      double vx = rho_dot * cos(phi);
  	  double vy = rho_dot * sin(phi);
      double v = sqrt(vx * vx + vy * vy);
      x_ << px, py, v, 0, 0;
    } else {
      x_ << meas_package.raw_measurements_[0], meas_package.raw_measurements_[1], 0, 0, 0;
    }

    // Saving first timestamp in seconds
    time_us_ = meas_package.timestamp_ ;
    // done initializing, no need to predict or update
    is_initialized_ = true;

    // std::cout << std::endl;
    return;
  }

  // Calculate dt
  double dt = (meas_package.timestamp_ - time_us_) / 1000000.0;
  time_us_ = meas_package.timestamp_;

  // Prediction step
  Prediction(dt);

  if (meas_package.sensor_type_ == MeasurementPackage::RADAR && use_radar_) {
    UpdateRadar(meas_package);
  }
  if (meas_package.sensor_type_ == MeasurementPackage::LASER && use_laser_) {
    UpdateLidar(meas_package);
  }

  // print the output
  std::cout << "x_ = " << x_ << std::endl;
  std::cout << "P_ = " << P_ << std::endl;

}

void UKF::Prediction(double delta_t) {
  /**
   * Estimate the object's location. 
   * Predict sigma points, the state, and the state covariance matrix.
   */

  // create augmented mean state
  VectorXd x_aug(7);
  x_aug.head(n_x_) = x_;
  x_aug.tail(2) << 0 , 0;

  // create augmented covariance matrix
  MatrixXd P_aug(7, 7);
  P_aug.topLeftCorner<5,5>() = P_;
  P_aug(n_x_,n_x_) = std_a_ * std_a_;
  P_aug(n_x_+1,n_x_+1) = std_yawdd_ * std_yawdd_;
  
  // create square root matrix
  MatrixXd A = P_aug.llt().matrixL();

  // create augmented sigma points
  MatrixXd Xsig_aug(n_aug_, 2 * n_aug_ + 1);
  Xsig_aug.block<7,1>(0,0) = x_aug;
  Xsig_aug.block<7,7>(0,1) = sqrt(lambda_+n_aug_) * A;
  Xsig_aug.block<7,7>(0,n_aug_+1) = -Xsig_aug.block<7,7>(0,1);
  Xsig_aug.block<7,2*7>(0,1).colwise() += x_aug;

  // useful quantity
  double delta_t_2 = delta_t*delta_t;

    // predict sigma points while avoiding division by zero
  Xsig_pred_.fill(0.0);

  for ( unsigned int i=0; i < 15; ++i )
  {
    if( fabs(Xsig_aug(4,i)) < 1e-5 ) // if this is 'almost' zero
    {
      Xsig_pred_(0,i) = Xsig_aug(2,i) * cos( Xsig_aug(3,i) ) * delta_t ;
      Xsig_pred_(1,i) = Xsig_aug(2,i) * sin( Xsig_aug(3,i) ) * delta_t ;
      Xsig_pred_(3,i) = 0. ;
    }else
    {
      Xsig_pred_(0,i) = Xsig_aug(2,i) / Xsig_aug(4,i) * ( sin( Xsig_aug(3,i) + Xsig_aug(4,i) * delta_t ) - sin( Xsig_aug(3,i) ) ) ;
      Xsig_pred_(1,i) = Xsig_aug(2,i) / Xsig_aug(4,i) * (-cos( Xsig_aug(3,i) + Xsig_aug(4,i) * delta_t ) + cos( Xsig_aug(3,i) ) ) ;
      Xsig_pred_(3,i) = Xsig_aug(4,i) * delta_t ;
    }
    Xsig_pred_(2,i) = 0 ;
    Xsig_pred_(4,i) = 0 ;
    
    Xsig_pred_(0,i) += 0.5 * delta_t_2 * cos( Xsig_aug(3,i) ) * Xsig_aug(5,i);
    Xsig_pred_(1,i) += 0.5 * delta_t_2 * sin( Xsig_aug(3,i) ) * Xsig_aug(5,i);
    Xsig_pred_(2,i) += delta_t * Xsig_aug(5,i);
    Xsig_pred_(3,i) += 0.5 * delta_t_2 * Xsig_aug(6,i);
    Xsig_pred_(4,i) += delta_t * Xsig_aug(6,i);
    
    Xsig_pred_.col(i) += Xsig_aug.block<5,1>(0,i);
  }
  
  // create vector for predicted state
  x_.fill(0.);
  for( int i=0; i<(2*n_aug_+1); ++i)
      x_ += weights_(i) * Xsig_pred_.col(i);
  
  // create covariance matrix for prediction
  P_.fill(0.0);
  for (int i = 0; i < 2 * n_aug_ + 1; ++i) {  // iterate over sigma points
      // state difference
      VectorXd t = Xsig_pred_.col(i) - x_;
      // angle normalization
      t(3) = NormalizeAngle(t(3));

      P_ += weights_(i) * t * t.transpose();
  }
}

void UKF::UpdateLidar(MeasurementPackage meas_package) {
  /**
   * Use lidar data to update the belief about the object's position. 
   * Modify the state vector, x_, and covariance, P_.
   * You can also calculate the lidar NIS, if desired.
   */

  // set measurement dimension, laser can measure px, py
  int n_z = 2;

  // create matrix for sigma points in measurement space
  MatrixXd Zsig = Xsig_pred_.block(0, 0, n_z, 2*n_aug_+1);
  // in this case there's no need to transform sigma points into measurement space -- only crop the matrix to the first two

  // mean predicted measurement
  VectorXd z_pred = VectorXd(n_z);
  // calculate mean predicted measurement
  z_pred.fill(0.);
  for ( int i=0 ; i < 2*n_aug_+1 ; ++i )
    z_pred += weights_(i) * Zsig.col(i);
    
  // measurement covariance matrix S
  MatrixXd S = MatrixXd(n_z,n_z);
  S.fill(0.);
  // create matrix for cross correlation Tc
  MatrixXd Tc = MatrixXd(n_x_, n_z);
  Tc.fill(0.);


  // calculate innovation covariance matrix S
  // calculate cross correlation matrix
  
  for ( int i=0 ; i < 2*n_aug_+1 ; ++i )
  {
    VectorXd z_diff = Zsig.col(i) - z_pred;
    VectorXd x_diff = Xsig_pred_.col(i) - x_;

    S += weights_(i) * z_diff * z_diff.transpose();
    Tc += weights_(i) * x_diff * z_diff.transpose();
  }

  S(0,0) += std_laspx_*std_laspx_;
  S(1,1) += std_laspy_*std_laspy_;

  // calculate Kalman gain K;
  MatrixXd K = Tc * S.inverse();

  // update state mean and covariance matrix based on observed measurements
  VectorXd z_meas_diff = ( meas_package.raw_measurements_ - z_pred );
  x_ += K * z_meas_diff;
  P_ -= K * S * K.transpose();

  //NIS Update
  NIS_laser_ = z_meas_diff.transpose() * S.inverse() * z_meas_diff;

}

void UKF::UpdateRadar(MeasurementPackage meas_package) {
  /**
   * Use radar data to update the belief about the object's position. 
   * Modify the state vector, x_, and covariance, P_.
   * You can also calculate the radar NIS, if desired.
   */

  // set measurement dimension, radar can measure r, phi, and r_dot
  int n_z = 3;

  // create matrix for sigma points in measurement space
  MatrixXd Zsig = MatrixXd(n_z, 2*n_aug_+1);  

  // transform sigma points into measurement space
  for ( int i=0 ; i < 2*n_aug_+1 ; ++i )
  {
      Zsig(0,i) = sqrt( Xsig_pred_(0,i)*Xsig_pred_(0,i) + Xsig_pred_(1,i)*Xsig_pred_(1,i) );
      Zsig(1,i) = atan2( Xsig_pred_(1,i) , Xsig_pred_(0,i) );
      Zsig(2,i) = ( Xsig_pred_(0,i) * cos( Xsig_pred_(3,i) ) * Xsig_pred_(2,i) + 
                    Xsig_pred_(1,i) * sin( Xsig_pred_(3,i) ) * Xsig_pred_(2,i) ) /
                    Zsig(0,i);
  }

  // mean predicted measurement
  VectorXd z_pred = VectorXd(n_z);
  // calculate mean predicted measurement
  z_pred.fill(0.);
  for ( int i=0 ; i < 2*n_aug_+1 ; ++i )
    z_pred += weights_(i) * Zsig.col(i);
    
  // measurement covariance matrix S
  MatrixXd S = MatrixXd(n_z,n_z);
  S.fill(0.);
  // create matrix for cross correlation Tc
  MatrixXd Tc = MatrixXd(n_x_, n_z);
  Tc.fill(0.);


  // calculate innovation covariance matrix S
  // calculate cross correlation matrix
  
  for ( int i=0 ; i < 2*n_aug_+1 ; ++i )
  {
    VectorXd z_diff = Zsig.col(i) - z_pred;
    VectorXd x_diff = Xsig_pred_.col(i) - x_;

    S += weights_(i) * z_diff * z_diff.transpose();
    Tc += weights_(i) * x_diff * z_diff.transpose();
  }

  S(0,0) += std_radr_*std_radr_;
  S(1,1) += std_radphi_*std_radphi_;
  S(2,2) += std_radrd_*std_radrd_;

  // calculate Kalman gain K;
  MatrixXd K = Tc * S.inverse();

  // update state mean and covariance matrix based on observed measurements
  VectorXd z_meas_diff = ( meas_package.raw_measurements_ - z_pred );
  x_ += K * z_meas_diff;
  P_ -= K * S * K.transpose();

  //NIS Update
  NIS_radar_ = z_meas_diff.transpose() * S.inverse() * z_meas_diff;

}