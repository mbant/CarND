# Kalman Filter Project
Self-Driving Car Engineer Nanodegree Program

In this project I'll code a Kalman filter to track a detected vehicle's position and trajectory.

In the EKF folder, an Extended Kalman Filter assuming a Constant Velocity (CV) process model is presented. The estimation procedure works ok for the simulator at hand but suffer from two drawbacks in real-world applications:
* Even if the state and measurement space is quite low-dimensional for now, having to compute the Jacobian at each timestep is computationally expensive;
* the CV model is not flexible enough to realistically represent vehicles on the road.

In the UKF folder then I'll present an Uscented Kalman Filter, to address the first point, and assume a **constant turn rate and velocity magnitude (CTRV)** model that will address the second drawback of the previous estimator.