#include "FusionEKF.h"
#include <iostream>
#include "Eigen/Dense"
#include "tools.h"

using Eigen::MatrixXd;
using Eigen::VectorXd;
using std::cout;
using std::endl;
using std::vector;

/**
 * Constructor.
 */
FusionEKF::FusionEKF() {
  is_initialized_ = false;

  previous_timestamp_ = 0;

  // initializing matrices
  R_laser_ = MatrixXd(2, 2);
  R_radar_ = MatrixXd(3, 3);
  H_laser_ = MatrixXd(2, 4);
  Hj_ = MatrixXd(3, 4);

  //measurement covariance matrix - laser
  R_laser_ << 0.0225, 0,
              0, 0.0225;
  
  //measurement covariance matrix - radar
  R_radar_ << 0.09, 0, 0,
              0, 0.0009, 0,
              0, 0, 0.09;
  
  // measurement matrix for laser
  H_laser_ << 1, 0, 0, 0,
          	  0, 1, 0, 0;
  
  // Hj depends on the state so it'll be properly initialised as needed.
  
  /**
   * TODO: Set the process noises
   * ??? here ? why ? process noise depends on dt
   */

}

/**
 * Destructor.
 */
FusionEKF::~FusionEKF() {}

void FusionEKF::ProcessMeasurement(const MeasurementPackage &measurement_pack) {
  /**
   * Initialization
   */
  if (!is_initialized_) {
    
    ekf_ = KalmanFilter();
    // I could use Init here but there are matrices that needs to be initialised later so...
    
    // timestamp
    previous_timestamp_ = measurement_pack.timestamp_ ;
    
    // state covariance matrix P
    ekf_.P_ = MatrixXd(4, 4);
    ekf_.P_ << 10, 0, 0, 0,
               0, 10, 0, 0,
               0, 0, 1000, 0,
               0, 0, 0, 1000;

    // first measurement
    cout << "EKF: " << endl;
    ekf_.x_ = VectorXd(4);

    if (measurement_pack.sensor_type_ == MeasurementPackage::RADAR) {
      // Convert radar from polar to cartesian coordinates 
      //         and initialize state.
      float px,py;
      px = measurement_pack.raw_measurements_[0] * cos ( measurement_pack.raw_measurements_[1] );
      py = measurement_pack.raw_measurements_[0] * sin ( measurement_pack.raw_measurements_[1] );

      ekf_.x_ << px, py, 1, 1;
      
    }
    else if (measurement_pack.sensor_type_ == MeasurementPackage::LASER) {
      // Initialize state.
      ekf_.x_ << measurement_pack.raw_measurements_[0], 
              measurement_pack.raw_measurements_[1], 
              1, 
              1;
    }

    // done initializing, no need to predict or update
    is_initialized_ = true;
    return;
  }
  
  
  // print
//   cout << endl << endl;
//   cout << "INITIAL  : x_ = " << ekf_.x_ << endl;
//   cout << "P_ = " << ekf_.P_ << endl;
//   cout << "TIMESTAMP : " <<measurement_pack.timestamp_ << endl;
  

  /**
   * Prediction
   */

  /**
   * Update the state transition matrix F according to the new elapsed time.
   * Time is measured in seconds.
   * Update the process noise covariance matrix.
   * I use noise_ax = 9 and noise_ay = 9 for your Q matrix.
   */

  // set the acceleration noise components
  float noise_ax = 9;
  float noise_ay = 9;
  
  // update dt
  float dt = (measurement_pack.timestamp_ - previous_timestamp_) / 1000000.0;
  previous_timestamp_ = measurement_pack.timestamp_;
  
  ekf_.F_ = MatrixXd(4, 4);
  ekf_.F_ << 1, 0, dt, 0,
             0, 1, 0, dt,
             0, 0, 1, 0,
             0, 0, 0, 1;
  
  float dt_2 = dt * dt;
  float dt_3 = dt_2 * dt;
  float dt_4 = dt_3 * dt;

  // set the process covariance matrix Q
  ekf_.Q_ = MatrixXd(4, 4);
  ekf_.Q_ << dt_4/4*noise_ax, 0, dt_3/2*noise_ax, 0,
   		     0, dt_4/4*noise_ay, 0, dt_3/2*noise_ay,
    	     dt_3/2*noise_ax, 0, dt_2*noise_ax, 0,
        	 0, dt_3/2*noise_ay, 0, dt_2*noise_ay;
  
  ekf_.Predict();
    
  // print
//   cout << endl << endl;
//   cout << "PREDICTION  : x_ = " << ekf_.x_ << endl;
//   cout << "P_ = " << ekf_.P_ << endl;

  /**
   * Update
   */

  /**
   * - Use the sensor type to perform the update step.
   * - Update the state and covariance matrices.
   */

  if (measurement_pack.sensor_type_ == MeasurementPackage::RADAR) {
    // Radar updates
    Hj_ = tools.CalculateJacobian(ekf_.x_);
    ekf_.R_ = R_radar_;
    ekf_.H_ = Hj_;
	ekf_.UpdateEKF(measurement_pack.raw_measurements_);
//     cout << "RADAR  :" ;
  } else {
    // Laser updates
    ekf_.R_ = R_laser_;
    ekf_.H_ = H_laser_;
	ekf_.Update(measurement_pack.raw_measurements_);
//     cout << "LASER  :" ;
  }

  // print the output
  cout << "x_ = " << ekf_.x_ << endl;
  cout << "P_ = " << ekf_.P_ << endl;
}
