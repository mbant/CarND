## Advanced Lane Finding
[![Udacity - Self-Driving Car NanoDegree](https://s3.amazonaws.com/udacity-sdc/github/shield-carnd.svg)](http://www.udacity.com/drive)

![Lanes Image](./output/example_output.png)

In this project, my goal was to write a software pipeline to identify the lane boundaries in a video, while also measuring curvature of the road and offset of the car from the center of the lane.

More precisely the goals / steps of this project are the following:

* Compute the camera calibration matrix and distortion coefficients given a set of chessboard images.
* Apply a distortion correction to raw images.
* Use color transforms, gradients, etc., to create a thresholded binary image.
* Apply a perspective transform to rectify binary image ("birds-eye view").
* Detect lane pixels and fit to find the lane boundary.
* Determine the curvature of the lane and vehicle position with respect to center.
* Warp the detected lane boundaries back onto the original image.
* Output visual display of the lane boundaries and numerical estimation of lane curvature and vehicle position.

[//]: # (Image References)

[image1.1]: ./output/undistort_output1.png "Undistorted"
[image1.2]: ./output/undistort_output2.png "Undistorted"
[image2]: ./output/undistort_road.png "Road Transformed"
[image3.1]: ./output/binary_combo_example.png "Binary Example"
[image3.2]: ./output/lvchannel.png "L and V Channels"
[image4]: ./output/warped_straight_lines.png "Warp Example"
[image5]: ./output/color_fit_lines.png "Fit Visual"
[image8]: ./output/example_output.png "Output"
[video1]: ./output/project_video.mp4 "Video"


### Camera Calibration

The code for this step is contained in IPython notebook "./Camera Calibration.ipynb".

I start in the first cell by preparing "object points", which will be the (x, y, z) coordinates of the chessboard corners in the real world. Here I am assuming the chessboard is fixed on the (x, y) plane (so $z=0$ always), such that the object points are the same for each calibration image.  Thus, `objp` is just a replicated array of coordinates, and `objpoints` will be appended with a copy of it every time I successfully detect all chessboard corners in a test image.  `imgpoints` will be appended with the (x, y) pixel position of each of the corners in the image plane with each successful chessboard detection using the `cv2.findChessboardCorners()` function.  

I then used the output `objpoints` and `imgpoints` to compute the camera calibration and distortion coefficients using the `cv2.calibrateCamera()` function.  I applied this distortion correction to the test image using the `cv2.undistort()` function and obtained this result: 

![Camera Calibration1][image1.1]
![Camera Calibration2][image1.2]

### Class structure

The following can all be found in the "./P2.ipynb" notebook.

I start in the first cell by defining the `Line` class that contains:
* indicators for whether the line was detected on the previous frame or not and, if not, how long till last detection;
* the current values for the detected `x` and `y` pixels that belong to it, the corresponding polynomial fits and the fitted values of `x`;
* the **recent** values (*i.e.* the saved values from a certain number of past iterations ) for the detected `x` and `y` pixels that belong to it, the corresponding polynomial fits and the fitted values of `x`; these will be used to smooth out the line fit, by pooling together past detection and fitting a more robust line.
* the radius of curvature of the line.

Then I introduce the `Lane` class, that keeps track of
* the two `Line`s, `left` and `right`;
* the road curvature;
* the offset of the car from the center of the lane;
* the current frame;
* other useful quantities, like the `dist`ortion coefficient and camera matrix, the source(`src`) and destination (`dst`) point for the perspective transform, and so on...

### Pipeline (single images)

#### Undistorting road images

Having `pickled` the resulting distortion coefficients and camera matrix from the camera calibration, I can simply apply the `cv2.undistort()` function to each frame to get the undistorted image ( line 53 to 55 of the second cell of `./P2.ipynb` )

![Undistorted road][image2]

#### Color transforms, gradients or other methods to create a thresholded binary image.

I used a combination of color and gradient thresholds to generate a binary image. 
The corresponding code can be found from line 65 to 90 of the main cell of `./P2.ipynb`.
As you can see I first transorm the image from RGB to HLS color space and subsequently bind together a thresholded S channel and the thresholded X-Sobol-Gradient of the L channel to obtain the final binary image. 
Here's an example of my output for this step.
![binary thresholded][image3.1]

As you can see both parts contribute significantly in identifying line edges and thus we use them in combination.

Another interesting channel emerges from transforming to HSV color space and obtaining the V channel. It performs at least as good as the L channel in a number of tests, as shown by the figure below, but thisrequires an extra color conversion at each step (since the S channel in HSV is not as good as the S in HLS for thresholding); I decided thus to use the L channel in the pipeline.

![alt text][image3.2]

#### 3. Perspective transform

From line 57 to 62, the `top_down_perspective()` function performs the perspective transform on road images.

I chose the hardcoded source and destination points in the following manner:

```python
src = np.float32(
    [[(img_size[1] / 2) - 55, img_size[0] / 2 + 100],
    [((img_size[1] / 6) - 10), img_size[0]],
    [(img_size[1] / 2 + 55), img_size[0] / 2 + 100],
    [(img_size[1] * 5 / 6) + 60, img_size[0]]])

dst = np.float32(
    [[(img_size[1] / 4), 0],
    [(img_size[1] / 4), img_size[0]],
    [(img_size[1] * 3 / 4), 0],
    [(img_size[1] * 3 / 4), img_size[0]]])
```

This resulted in the following source and destination points for an `1280x720` pixel image like the one in the project video:

| Source        | Destination   | 
|:-------------:|:-------------:| 
| 585, 460      | 320, 0        | 
| 203, 720      | 320, 720      |
| 695, 460      | 960, 0        |
| 1225, 720     | 960, 720      |

This values get used by the `cv2.getPerspectiveTransform()` (lines 29-33) to derive the `M` transformation matrix (and itsinverse `Minv`) when I create the `Lane` object.

I verified that my perspective transform was working as expected by drawing the `src` and `dst` points onto a test image and its warped counterpart to verify that the lines appear parallel in the warped image.

![warped road][image4]

Using the transformed image and the regulation regarding lane width and dashed lines length, I was also able to deduce that (in the transformed space) we are using `720` pixels to represent ~$32$m (the whole vertical span) and, since I assumed the lane is around $3.7$m wide, `640` pixels to represent $3.7$m; I used these to define 

```
xm = 3.7/(dst[2][0] - dst[0][0]) # meters per pixel in x dimension
ym = 32/img_size[0] # meters per pixel in y dimension
```

#### 4. Identifying lane-line pixels and fit their positions with a polynomial

By warping the binary thresholded image, I can now idenitify pixels that belong to the lines. The fist fit and every subsequent detection if the `Line` was not found in the previous (few) frame(s) is done by sliding windows and histograms.

The code can be found in `search_from_scratch()` from line 120.
I collapse the last third of the image using `histogram = np.sum(binary_warped[3*binary_warped.shape[0]//4:,:], axis=0)` and then search around the left and right half of it for the most probable center of the line in the bottom part of the image; I center the first window there on the bottom of the image and then (from line 151) I draw a rectagular window, mark all nonzero pixels I find inside and assume they come from the detected line. 

Iteratively I proceed by re-centering the subsequent window based on the current detection and marking all pixel detected. If I don't find enough points I leave the center as it was and try to detect more pixels directly above$.^*$

I tested convolutions as well, which provide a bit of smoothing in the x direction, but the results were not significantly different.

Having detected
```
self.left.current_detected_x
self.left.current_detected_y
self.right.current_detected_x
self.right.current_detected_y
```

I use those points in the ` np.polyfit(self.left.current_detected_y,self.left.current_detected_x,2)` function, similarly for `right` to estimate a second degree polynomial for the line.

The result is like in the image below.
![histogram][image5]

If the line (either `left` or `right`) was detected in the previous frame tough, I first try to find a significant amount of pixels around the previously fitted polynomial.

This in done in the `search_around_poly()` function from line 209, where I simply draw margins around the previous fit and mark all non-zero pixels inside as detections. If enough are found, I use those to fit the current `Line`.

This helps in speeding up the already quite slow pipeline avoiding the need to loop for `nwindows` at each frame and most importantly makes use of the idea that frm frame to frame the lines shoudn't change too much.

The function `find_lane_pixels()` from line 255 then wraps all this up after the necessary checks. The polynomail fit is done in `update_current_fit()` from line 429.

$^*$ I suspect this would fail the harder challenge video, as the  curvature radius of some turns is so high that the lanes "exit" the image from a side rather than from the top. Adding a check to see if the current window is already at the edge of the image, and in that case consider the detection finished, might help alleviate that problem.

#### 5. Sanity checks

After fitting in the `update_current_fit()` function is done, some `sanity_checks()` (from line 403) are performed to ensure that the current detection was in line with what we'd expect.

We measure curvature of both lines and see that they don't differ too much, we check that at the bottom of the image the two fitted lines are at least $3$m apart, and finally we check that the two lines don't diverge from one another as a proxy for checking on them being parallel.

If any of those checks fails, we `reset()` the current fit and rely on past fits to inform the current frame instead. This will trigger the sliding windows detection on the subsequent frame if the past detections are too far in the past.

#### 6. Smoothing the line detections

Even if we detect the lines' pixels around the previous fit, due to outliers, changes in the dashed lines and many other factors, the detected line will jitter from frame to frame. To alleviate this issue I keep track not only of the detection made on this frame, but also of a number of past detection (this number corresponf to the `lane.n` parameters and can be changed while first creating the object `Lane`).

Essentially I keep track of the `current_frame` and use the modulo operator to cycle from $0$ to `self.n-1` and save the detections in a list, replacing the older as newer ones comes in. I then pool all the points together using `np.concatenate` and fit the current polynomial, the road curvature and the offset using all those past (and current) detections.

I tried averaging the previous fits as well, but this leads usually to higher variability in the estimates than the above procedure.

This is done in `update_best_fit()` from line 445, alongside some checks to ensure that no error is produced.

#### 7. Radius of curvature of the lane and the position of the vehicle with respect to center.

The `measure_curvature()` and `measure_offset()` functions ( resp. line 327 and 350 ) take care of estimating these two quantities. 

Given a polynomial fit $Ay^2 + By + C$ for either line, to compute the radius of curvature I use the formula 

$R = \left(1+\left(2 A_{rw} y_0 + B_{rw}\right)^2 \right)^{3/2} / \|2A_{rw}\|$

where $y_0$ is the bottom point in the image, measured in meters, $A_{rw} = A\frac{x_m}{y_m^2}$ and $B_{rw} = B \frac{x_m}{y_m}$; the suffix $rw$ stands for real-world and $x_m,y_m$ are respectively the meters-per-pixel on the $x$ and $y$ axis.

I then average the two (obtained from the two lines) into the quantity that will be printed on screen later on.

As for the offset, I first compute the bottom $x$-value of each line, called $x_0$, using the polynomial fit (which is simply the point corresponding to the bottom of the **warped** image), then use the following formula alongside the `Minv` matrix (the inverst perspective-transformation matrix, obtained by inverting source and destination points while calling `cv2.getPerspectiveTransform()`) to warp the $x$-points back to the orginal space

$x_{0,warp} = \frac{M^{inv}_{0,0} x_0 + M^{inv}_{0,1} y_0 + M^{inv}_{0,2} }{ M^{inv}_{2,0} x_0 + M^{inv}_{2,1} y_0 + M^{inv}_{2,2} }$

Now, the difference between the mean point of the $x_{0,warp}$ for the left and right line and the center of the image on the $x$-axis gives us the offset of the car from the center of the lane **in pixels**; we only need to multiply it by `xm_per_pix` to get the result in meters.


#### 8. Example image of the result, plotted back down onto the road.

In the function `draw_final()` (line 467 on), I finally plot the result back onto the original undistorted image and highlight the lane found, as well as the estimated curvature and offset.

By using `cv2.fillPoly()` I draw a green mask on the perspective-transformed between the two estimated polynomial for the two lines. I then use `cv2.warpPerspective` with the `Minv` inverse transformation matrix to transform it back to the original space and `cv2.addWeighted()` to superimpose it with some transparency to the original image.

Finally, `cv2.putText()` is used to write on the top left corner the other relevant information, after computing that the curvature is within a sensibe range (or "Negligible" is printed for stright roads) and wheter the car is to the left or to the right of the center of the lane.

Here is an example of the output on a test image:
![alt text][image8]

---

### Pipeline (video)

The same pipeline I just described, defined on line 540 onwards of the second cell of `./P2.ipynb`, is then applied to the `project_video.mp4` video; here's a [link to my video result](./output/project_video.mp4).

---

### Discussion

I initially set by mistake an asymmetric transformation points `src` and thus the curvature of the lines were a lot different from left to right lane and that made so the safety checks were failing continously. Thinkg about the fact that the road is not in reality a flat plane as assumed here, a real working line detection algorithm will probably need to pick up those informations from the frames or from other sensors as well!

At the end of the curves on the road, because of the averaging, the line takes a fraction of a second to get back to straight. I might improve on this by weighting nearer (in time) fits more than previous ones, so that the current shape of the road counts more when computing `best_fit`.

See $^*$ at point 4, to correct for a problem regarding sharp turns.