/**
 * particle_filter.cpp
 *
 * Created on: Dec 12, 2016
 * Author: Tiffany Huang
 */

#include "particle_filter.h"

#include <math.h>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <numeric>
#include <random>
#include <string>
#include <vector>

#include "helper_functions.h"

using std::string;
using std::vector;

void ParticleFilter::init(double x, double y, double theta, double std[]) {
  /**
   * Initialize all particles to 
   *   first position (based on estimates of x, y, theta and their uncertainties
   *   from GPS) and all weights to 1. 
   * Add random Gaussian noise to each particle.
   */

  num_particles = 100;

  // This line creates a normal (Gaussian) distribution for x,y and yaw
  std::default_random_engine gen;
  std::normal_distribution<double> dist_x(x, std[0]);
  std::normal_distribution<double> dist_y(y, std[1]);
  std::normal_distribution<double> dist_theta(theta, std[2]);

  for (int i=0; i<num_particles; ++i)
  {
    Particle p;
    p.id = i;
    p.x = dist_x(gen);
    p.y = dist_y(gen);
    p.theta = dist_theta(gen);
    p.log_weight = 0.0;

    particles.push_back(p);
  }

  is_initialized = true;
}

void ParticleFilter::prediction(double delta_t, double std_pos[], 
                                double velocity, double yaw_rate) {
  /**
   * Add measurements to each particle and add random Gaussian noise.
   */

  // Distributions for Gaussian noise additive to velocity and yaw rate
  std::default_random_engine gen;
  std::normal_distribution<double> dist_x(0.0, std_pos[0]);
  std::normal_distribution<double> dist_y(0.0, std_pos[1]);
  std::normal_distribution<double> dist_theta(0.0, std_pos[2]);

  for (auto& p : particles)
  {
    if ( std::abs(yaw_rate) > 1e-3 )
    {
      p.x = p.x + velocity/yaw_rate*( sin(p.theta + yaw_rate*delta_t) - sin(p.theta) ) + dist_x(gen);
      p.y = p.y + velocity/yaw_rate*( cos(p.theta) - cos(p.theta + yaw_rate*delta_t) ) + dist_y(gen);
      p.theta = p.theta + yaw_rate*delta_t + dist_theta(gen);
    }
    else
    {
      p.x = p.x + (velocity*delta_t)*( cos(p.theta) ) + dist_x(gen);
      p.y = p.y + (velocity*delta_t)*( sin(p.theta) ) + dist_y(gen);
      p.theta = p.theta + dist_theta(gen);      
    }
  }

}

void ParticleFilter::dataAssociation(vector<LandmarkObs> predicted, 
                                     vector<LandmarkObs>& transformed_observations) {
  /**
   * Find the predicted measurement that is closest to each 
   *   observed measurement and assign the observed measurement to this 
   *   particular landmark.
   */

  unsigned int n_obs = transformed_observations.size();
  unsigned int n_pred = predicted.size();

	for (auto& o : transformed_observations) 
  {
    double min_dist = std::numeric_limits<double>::max();
    int map_id {-1};
    for (auto const& p : predicted)
    {
      double distance = dist(o.x,o.y,p.x,p.y);

      if(distance < min_dist) 
      {
        min_dist = distance;
        map_id = p.id;
      }
      o.id = map_id;
    }
  }

}

LandmarkObs transformObservation(const Particle& p, const LandmarkObs& l) {
  /**
   * Transform observations in the car coordinate system into map coordinates
   * xm​=xp​+(cosθ×xc​)−(sinθ×yc​)
   * ym=yp+(sinθ×xc)+(cosθ×yc)
   */
    double xm = p.x + cos(p.theta)*l.x - sin(p.theta)*l.y ;
    double ym = p.y + sin(p.theta)*l.x + cos(p.theta)*l.y ;

    return LandmarkObs{ l.id , xm, ym };
}

void ParticleFilter::updateWeights(double sensor_range, double std_landmark[], 
                                   const vector<LandmarkObs> &observations, 
                                   const Map &map_landmarks) {
  /**
   * Update the weights of each particle using a multivariate Gaussian 
   *   distribution. 
   * NOTE: The observations are given in the VEHICLE'S coordinate system 
   *   while particles are located according to the MAP'S coordinate system. 
   */

  for (auto& p : particles)
  {
    // Convert the landmark observation in map coordinates for this particle
    vector<LandmarkObs> trans_landmarks;
    for(auto const& o : observations)
      trans_landmarks.push_back(transformObservation(p, o));

    // Predict which map landmarks are within sensor_range from this particle
    vector<LandmarkObs> predicted;
    for(auto const& l : map_landmarks.landmark_list)
      if ( dist(p.x,p.y,l.x_f,l.y_f) <= sensor_range ) // circular
        predicted.push_back( LandmarkObs{l.id_i,l.x_f,l.y_f} );

    // Associate each observation with a real landmark via NN
    dataAssociation(predicted, trans_landmarks);

    // set these associations for debugging and later use
    vector<int> assoc;
    vector<double>sense_x;
    vector<double>sense_y;
    vector<double>map_x;
    vector<double>map_y;
    for (auto const& l : trans_landmarks)
    {
      assoc.push_back(l.id);
      sense_x.push_back(l.x);
      sense_y.push_back(l.y);

      for (auto const& pl : predicted)
      {
        if (pl.id == l.id)
        {
          map_x.push_back(pl.x);
          map_y.push_back(pl.y);
          break;
        }
      }
    }
    setAssociations(p, assoc,sense_x,sense_y);

    // Compute the log likelihood for the particle
    double ll{0.0};
    for(int i=0; i<trans_landmarks.size(); ++i)
      ll += log_multiv_gauss(sense_x[i],sense_y[i],map_x[i],map_y[i],std_landmark[0],std_landmark[1]);

    p.log_weight = ll;
  }

}

void ParticleFilter::resample() {
  // multinomial_resample();
  wheel_resample(); // choose one
}

void ParticleFilter::wheel_resample() {

  //Get weights and max weight.
  double maxWeight = std::numeric_limits<double>::lowest();

  vector<double> log_weights;
  log_weights.reserve(num_particles);
  for(auto const& p : particles)
    log_weights.push_back(p.log_weight);

  // Normalise the weights
  double weight_sum = logsumexp(log_weights);
  for(auto& lw : log_weights)
    lw -= weight_sum;

  vector<double> weights; weights.reserve(num_particles);
  for(auto const& lw : log_weights)
  {
    double elw = exp(lw);
    weights.push_back(elw);
    if (elw > maxWeight)
      maxWeight = elw;
  }

  w = weights;

  std::default_random_engine gen;
  std::uniform_real_distribution<double> distReal(0.0, maxWeight);
  std::uniform_int_distribution<int> distInt(0, num_particles - 1);

  int index = distInt(gen);
  double beta = 0.0;
  vector<Particle> resampledParticles;
  for(int i = 0; i < num_particles; i++) {
    beta += distReal(gen) * 2.0;
    while(beta > weights[index]) {
      beta -= weights[index];
      index = (index + 1) % num_particles;
    }
    resampledParticles.push_back(particles[index]);
  }

  particles = resampledParticles;
}

void ParticleFilter::multinomial_resample() {
  /**
   * Resample particles with replacement with probability proportional to their weight. 
   */
  vector<double> log_weights;
  log_weights.reserve(num_particles);
  for(auto const& p : particles)
    log_weights.push_back(p.log_weight);

  // Normalise the weights
  double weight_sum = logsumexp(log_weights);
  for(auto& lw : log_weights)
    lw -= weight_sum;

  vector<double> weights; weights.reserve(num_particles);
  for(auto const& lw : log_weights)
    weights.push_back(exp(lw));

  w = weights;

  // Init the discrete distribution
  std::default_random_engine gen;
  std::discrete_distribution<> d(weights.begin(), weights.end());

  // Resample
  std::vector<Particle> new_particles; 
  new_particles.reserve(num_particles);
  for (int i=0; i<num_particles; ++i)
  {
    new_particles.push_back( particles[d(gen)] );
    new_particles[i].log_weight = 0.0; // reset its weight
  }

  particles = new_particles;
}

void ParticleFilter::setAssociations(Particle& particle, 
                                     const vector<int>& associations, 
                                     const vector<double>& sense_x, 
                                     const vector<double>& sense_y) {
  // particle: the particle to which assign each listed association, 
  //   and association's (x,y) world coordinates mapping
  // associations: The landmark id that goes along with each listed association
  // sense_x: the associations x mapping already converted to world coordinates
  // sense_y: the associations y mapping already converted to world coordinates
  particle.associations = associations;
  particle.sense_x = sense_x;
  particle.sense_y = sense_y;
}

string ParticleFilter::getAssociations(Particle best) {
  vector<int> v = best.associations;
  std::stringstream ss;
  copy(v.begin(), v.end(), std::ostream_iterator<int>(ss, " "));
  string s = ss.str();
  s = s.substr(0, s.length()-1);  // get rid of the trailing space
  return s;
}

string ParticleFilter::getSenseCoord(Particle best, string coord) {
  vector<double> v;

  if (coord == "X") {
    v = best.sense_x;
  } else {
    v = best.sense_y;
  }

  std::stringstream ss;
  copy(v.begin(), v.end(), std::ostream_iterator<float>(ss, " "));
  string s = ss.str();
  s = s.substr(0, s.length()-1);  // get rid of the trailing space
  return s;
}