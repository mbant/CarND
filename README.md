# Udacity Self Driving Car Engineer NanoDegree

[![Udacity - Self-Driving Car NanoDegree](https://s3.amazonaws.com/udacity-sdc/github/shield-carnd.svg)](http://www.udacity.com/drive)

Self Driving Cars NanoDegree from Udacity, navigate to each folder for each specific project:

* [Lane Lines Detection](https://gitlab.com/mbant/CarND/tree/master/LaneLines-P1) - using basic Computer Vision
* [Advanced Lane Lines Detections](https://gitlab.com/mbant/CarND/tree/master/AdvLaneLines-p2) - adding perspective transform, color gradients and polynomial fitting
* [Behavioral Cloning](https://gitlab.com/mbant/CarND/tree/master/Behavioral-Cloning-P3) - make a car drive itself in a simulator using Deep Learning
* [Tracking with Kalman Filters](https://gitlab.com/mbant/CarND/tree/master/KalmanFilter-P4) - Use an EFK to track a vehicle position; then an Unscented KF and a more realistic constant turn rate and velocity magnitude (CTRV) model to improve on it
* [Localisation with a Particle Filter](https://gitlab.com/mbant/CarND/tree/master/Localisation-P5) - Use a PF to help a car localise itself relative to predefined landmarks on the map
* [Path Planning](https://gitlab.com/mbant/CarND/tree/master/Path-Planning-P6) - Navigate a car around a virtual highway and plan its jerk-minimising paths as it speeds (below the speed limit) around and **safely** passes other car in its way
* [PID Controller](https://gitlab.com/mbant/CarND/tree/master/PID-Controller-P7) - Implement a PID controller and an optimisation procedure to tune its parametes
* [Capstone Project](https://gitlab.com/mbant/sdcarnd-capstone) - System Integration - Code a ROS system to drive Carla, Udacity's real self deriving car! 
    * also implements a [traffic light detection SSD Network](https://gitlab.com/mbant/sdcarnd-capstone/tree/master/obj_detection) to correctly stop at red lights!


:scroll: [Here's the GRADUATION CERTIFICATE](https://confirm.udacity.com/4SPMKE5) :tada: