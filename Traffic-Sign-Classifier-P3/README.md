# **Traffic Sign Recognition** 

## Writeup

---

**Build a Traffic Sign Recognition Project**

The goals of this project are the following:
* Load the data set (see below for links to the project data set)
* Explore, summarize and visualize the data set
* Design, train and test a model architecture
* Use the model to make predictions on new images
* Analyze the softmax probabilities of the new images


[//]: # (Image References)

[image1.0]: ./examples/sign.png "Example Sign"
[image1.1]: ./examples/hist_norm.png "Histogram Brightness"
[image1.2]: ./examples/most_freq.png "Class Frequency"
[image1.3]: ./examples/least_freq.png "Class Frequency"

[image1.4]: ./examples/bar_train.png "Class Frequency"
[image1.5]: ./examples/bar_valid.png "Class Frequency"
[image1.6]: ./examples/bar_test.png "Class Frequency"

[image1.7]: ./examples/extra_data.png "Extra Data"

[image2.1]: ./examples/viz_l1.png "Viz Layer_1"
[image2.2]: ./examples/viz_l2.png "Viz Layer_2"


[image4]: ./extra/1.jpg "Traffic Sign 1"
[image5]: ./extra/2.jpg "Traffic Sign 2"
[image6]: ./extra/3.jpg "Traffic Sign 3"
[image7]: ./extra/4.jpg "Traffic Sign 4"
[image8]: ./extra/5.jpg "Traffic Sign 5"

### Data Set Summary & Exploration

#### 1. Dataset Dimensions

I used the numpy library to calculate summary statistics of the traffic signs data set; overall we have:

* A total of 43 sign-types to classify
* The size of training set is 34799
* The size of the validation set is 4410
* The size of test set is 12630
* The shape of a traffic sign image is 32 pixels by 32 pixels; they are RGB image (hence 3 channels)

#### 2. Exploratory visualization of the dataset.

Here are some example images from the dataset, alongside their label in the corner:

![alt text][image1.0]

As we can see some images are quite dark, while some are overexposed. We could think about normalising their brightness during preprocessing as shown below

![alt text][image1.1]

This definitely increases contrast and thus helps ( thinking as a human, who knows what the NN prefers... ) but it could lead to some artifacts as well, so we'll test its efficacy and not apply it blindly.

Finally let's have a look at the class distribution using `pandas`:

![alt text][image1.2]
![alt text][image1.3]

There's a lot of discrepancy, with the least represented classes (20 km/h speed limit, dangerous curves, ... ) having as far as 10 times less instances than the most represented ones ( 50 or 30 km/h speed limit , ... )
Let's see an histogram!

![alt text][image1.4]

Similar patterns can be seen in the validation and test sets respectively:

![alt text][image1.5]
![alt text][image1.6]


#### 2.1 Correcting this class bias

We will definitely oversample the data by transforming the existing images and applying random transformations to them before feeding the data to the NN. We might want to correct this bias toward the more frequent signs by sampling more images for the least represented classes and less for the already frequent one.

This bias though is probably not random, but symbolise a real difference in frequency of signals on the street overall; so we don't want to correct it all the way to equalise all the class frequencies. Moreover, because there's such a wide difference, obtaining a uniform frequency of say twice as many as the most represented classes would lead to resample on average more than 20 times each single image in the least represented classes, leading the NN to almost interpolate those image and possibly performing badly on new images of that class.

So we will oversample more for the least represented classes but we won't aim for uniform class distribution.


### Design and Test a Model Architecture

#### 1. Data preprocessing:

First, we will convert the images from RGB to YUV, as suggested by [the paper](http://yann.lecun.com/exdb/publis/pdf/sermanet-ijcnn-11.pdf), then use histogram equalisation to standardise the brightness in the image and finally normalise all layers to the [0,1] range.

Note we do not fully convert to grayscale, but we'll use mostly the brightness channel (see architecture below), which could be interpreted as a special case of grayscale image.

#### 1.1 Data Augmentation

Before feeding the images to the NN, we will augment the size of the training dataset by applying some transformation to images so that that the network learns not to rely on brightness or perspective to classify the images. We will also sample more from least represented classes.

We will thus sample extra data by randomly translating, rotating ( in a certain range, we don't want to end up flipping a left/right sign ) and shifting ( to simulate change in perspective ) the base image. 

Here's a few examples for one random traffic sign:

![alt text][image1.7]

The final dimension of the training dataset is now of 117119 samples.

#### 2. Model Architecture

Starting from the LeNet-5 architecture used in the Tensorflow introduction, I added a few convolutional layers to improve performances, added dropout to a couple key layers and added a regularisation calculation. More details in the project writeup.

Note that I do not use the grayscalechannel only, but at the first layer I use 20 convolutional filters on the brightness channel alone and only 4 on the UV channels that contain the color information. This is because after reading [the paper](http://yann.lecun.com/exdb/publis/pdf/sermanet-ijcnn-11.pdf) I expect the Y channel to contain most of the information, but still completely disregarding color could hinder the recognition of a few images in the set.

My final model consisted of the following layers:

| Layer         		|     Description	        					| 
|:---------------------:|:---------------------------------------------:| 
| Input         		| 32x32x3 RGB image   							| 
| Convolution 3x3     	| input 32x32x1 Y input channel, 1x1 stride, valid padding, outputs 30x30x20 	|
| RELU					|												|
| Convolution 3x3     	| input 32x32x2 U ad V input channels, 1x1 stride, valid padding, outputs 30x30x4 	|
| RELU					|					|
| Concatenation of all the 24 filters applyed to input					|		|
| Max pooling	      	| 2x2 stride,  outputs 15x15x24 				|
| Convolution 3x3     	| 1x1 stride, valid padding, outputs 13x13x32 	|
| Average Pooling	      	| 2x2 stride,  outputs 12x12x32 				|
| Convolution 3x3     	| 1x1 stride, valid padding, outputs 10x10x64 	|
| Droupout |  keep_prob = 0.75 |
| Convolution 3x3     	| 1x1 stride, same padding, outputs 10x10x128 	|
| Max pooling	      	| 2x2 stride,  outputs 5x5x128 				|
| Fully connected		| 3200 flattend as input, output 1024        						|
| RELU					|					|
| Droupout |  keep_prob = 0.75 |
| Fully connected		| output 600        						|
| RELU					|					|
| Droupout |  keep_prob = 0.75 |
| Fully connected		| output 43 **logits**        						|
| Softmax				|									|
| Regularisation				| L2 norm of all the fully-connected layer's weights		|
 


#### 3. Describe how you trained your model. The discussion can include the type of optimizer, the batch size, number of epochs and any hyperparameters such as learning rate.

To train the model, I used Adam, with batch size of 64 and learning rate of 0.001. The loss function was the usual cross entropy  (between 1-hot-encoded labels and the softmaxed logits computed by the forward pass of the network) augmented with a penalisation $\lambda = 10^{-4}$ for the L2 norm of the weight, in order to favor sparser networks and hopefully end up with more zero weights.

As for epochs, I set a max number of epochs to $50$, but the training procedure was set to that we'd monitor the validation error in order to stop once it started decreasing again after reaching its maximum point. I used a `patience` value of 5 so that small (negative) variations in validation accuracy between iterations would not stop the training if the error started to decreaase again.

#### 4. Describe the approach taken for finding a solution and getting the validation set accuracy to be at least 0.93. Include in the discussion the results on the training, validation and test sets and where in the code these were calculated. Your approach may have been an iterative process, in which case, outline the steps you took to get to the final solution and why you chose those steps. Perhaps your solution involved an already well known implementation or architecture. In this case, discuss why you think the architecture is suitable for the current problem.

My final model results were:
* training set accuracy of 99.6%
* validation set accuracy of 99.3%
* test set accuracy of 97.7%

Using the validation set only, I tested quite a few architectures and multiple tuning parameters values. This was my first project in deep Learning and I wanted to get a sense on how different architectures would perform.

For example, I tried to build an inception module as follows:

    def inception2d( x , W1b1,W3b3_0,W3b3_1,W5b5_0,W5b5_1,Wap , b , k ):

        # 1x1
        one_by_one = tf.nn.conv2d(x, W1b1, strides=[1, 1, 1, 1], padding='SAME')

        # 3x3
        three_by_three = tf.nn.conv2d(x, W3b3_0, strides=[1, 1, 1, 1], padding='SAME')
        three_by_three = tf.nn.relu(three_by_three)
        three_by_three = tf.nn.conv2d(three_by_three, W3b3_1, strides=[1, 1, 1, 1], padding='SAME')

        # 5x5
        five_by_five = tf.nn.conv2d(x, W5b5_0, strides=[1, 1, 1, 1], padding='SAME')
        five_by_five = tf.nn.relu(five_by_five)
        five_by_five = tf.nn.conv2d(five_by_five, W5b5_1, strides=[1, 1, 1, 1], padding='SAME')

        # avg pooling
        pooling = tf.nn.avg_pool(x, ksize=[1, k, k, 1], strides=[1, 1, 1, 1], padding='SAME')
        pooling = tf.nn.conv2d(pooling, Wap, strides=[1, 1, 1, 1], padding='SAME')

        x = tf.concat([one_by_one, three_by_three, five_by_five, pooling], axis=3)  # Concat in the 4th dim to stack

        x = tf.nn.bias_add(x, b)

        return tf.nn.relu(x)

but the network using it didn't end up being competitive; one possibility for this is that because the images are only 32x32, this level of multiresolution is not particularly beneficial.

This is supported by the fact that I tested using multiresolution as described in [this paper](http://yann.lecun.com/exdb/publis/pdf/sermanet-ijcnn-11.pdf) as well, by feeding to the final fully-connected layers the output of an early convolutional layer, but the results didn't seem to increase considerably.

I tested using less than and more than two fully-connected layers, two seemed to be the sweet spot.

More layers of convolutions and different filter sizes and depths were tested as well, with bigger networks struggling with overfitting and smaller one not being able to classify well even on the training data.

Both dropout and the L2 regularisation seemed to help and are thus present in the final model. 

### Test a Model on New Images

#### 1. Five extra German traffic signs found on the web

Here are five German traffic signs that I found on the web:

![alt text][image4] ![alt text][image5] ![alt text][image6] 
![alt text][image7] ![alt text][image8]

Image number 3 is a 'slippery road' sign, and not many quality training examples are present in the original dataset.
Before the data augmentation step, its classification accuracy was in fact quite low:

![3_difficult](examples/3_first.jpeg)

I expect image 5 to possibly be challenging as well, I chose it because even if the 'priority road' sign is well represented, this image has a backward sign and road name sign stacked on top / near the main traffic sign we want to classify ( the priority road facing forward ) and thus the net might have troubles classifiying it correctly. This image would surely be challenging for a computer-vision type algorithm that relies on edge detections to extrapolate the boundaries of the sign!

#### 2. Discuss the model's predictions on these new traffic signs and compare the results to predicting on the test set. At a minimum, discuss what the predictions were, the accuracy on these new predictions, and compare the accuracy to the accuracy on the test set (OPTIONAL: Discuss the results in more detail as described in the "Stand Out Suggestions" part of the rubric).

The code for making predictions on my final model is located in the 25th cell of the Ipython notebook.
Here are the results of the prediction:

![extra_prediction](examples/extra_predictions.png)

As can be seen from the above image, the network obtains 100% accuracy on these 5 extra samples even the hard-to-classify slippery road sign and the priority sign stacked on top of another round sign are very confidently classified correctly after augmenting the dataset.

### Visualizing the Neural Network
#### 1. Visual output of the trained network's feature maps.

By feeding the network with the priority sign used in last section we can look at the inner network feature maps activations and see what the NN is "looking for" in the image.

![Viz Layer_1][image2.1]

It appears to me that the first layer (above) is mostly looking for the sign's boundary outline, while another subsequent convolutional layer (below) is cheking the interior of the sign, maybe for inner symbols or just colour differencies.
As expected, the last 4 filters ( that use color images as input ) pickup sort-of color regions in the image, separating the interior from the exterior of this priority sign. 

![Viz Layer_2][image2.2]

This last observation is even more evident if we feed an unrelated image to the network

![horse](examples/horse.png)

were is evident that the network split colours in the last 4 filters and gets 'excited' about diagonal and round edges in the the grayscale filters.
