#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "json.hpp"
#include <uWS/uWS.h>

#include "eigen3/Eigen/Core"
#include "eigen3/Eigen/QR"
#include "helpers.h"
#include "spline.h"

// for convenience
using nlohmann::json;
using std::string;
using std::tuple;
using std::vector;

int main() {
  uWS::Hub h;

  // Load up map values for waypoint's x,y,s and d normalized normal vectors
  vector<double> map_waypoints_x;
  vector<double> map_waypoints_y;
  vector<double> map_waypoints_s;
  vector<double> map_waypoints_dx;
  vector<double> map_waypoints_dy;

  // Waypoint map to read from
  const string map_file_ = "../data/highway_map.csv";
  // The max s value before wrapping around the track back to 0
  constexpr double max_s = 6945.554;

  std::ifstream in_map_(map_file_.c_str(), std::ifstream::in);

  string line;
  while (getline(in_map_, line)) {
    std::istringstream iss(line);
    double x;
    double y;
    float s;
    float d_x;
    float d_y;
    iss >> x;
    iss >> y;
    iss >> s;
    iss >> d_x;
    iss >> d_y;
    map_waypoints_x.push_back(x);
    map_waypoints_y.push_back(y);
    map_waypoints_s.push_back(s);
    map_waypoints_dx.push_back(d_x);
    map_waypoints_dy.push_back(d_y);
  }

  // TODO REFACTOR IN CAR OBJECT
  int lane = 1;
  // mph
  double speed_limit = 49.; // 49.5;
  double ref_speed = 0.;  
  // set it back to 0.0 to avoid initial jerk or speed_limit for testing

  h.onMessage([&map_waypoints_x, &map_waypoints_y, &map_waypoints_s,
               &map_waypoints_dx, &map_waypoints_dy, &lane, &ref_speed,
               &speed_limit](uWS::WebSocket<uWS::SERVER> ws, char *data,
                             size_t length, uWS::OpCode opCode) {
    // "42" at the start of the message means there's a websocket message event.
    // The 4 signifies a websocket message
    // The 2 signifies a websocket event
    if (length && length > 2 && data[0] == '4' && data[1] == '2') {

      auto s = hasData(data);

      if (s != "") {
        auto j = json::parse(s);

        string event = j[0].get<string>();

        if (event == "telemetry") {
          // j[1] is the data JSON object

          // Main car's localization Data
          double ego_car_x = j[1]["x"];
          double ego_car_y = j[1]["y"];
          double ego_car_s = j[1]["s"];
          double ego_car_d = j[1]["d"];
          double ego_car_yaw = j[1]["yaw"];
          double ego_car_speed = j[1]["speed"];

          // Previous path data given to the Planner
          auto previous_path_x = j[1]["previous_path_x"];
          auto previous_path_y = j[1]["previous_path_y"];
          // Previous path's end s and d values
          double end_path_s = j[1]["end_path_s"];
          double end_path_d = j[1]["end_path_d"];

          // Sensor Fusion Data, a list of all other cars on the same side
          //   of the road.
          auto sensor_fusion = j[1]["sensor_fusion"];

          /**
           * Define a path made up of (x,y) points that the car will
           * visit sequentially every .02 seconds
           */

          // More complex path, following the lane at a target speed
          size_t prev_path_size = previous_path_x.size();
          size_t planned_path_size = 50;

          if (prev_path_size > 0) {
            ego_car_s = end_path_s;
          }

          bool too_close = false;
          double too_close_meters_in_front = 30.;
          double too_close_meters_behind = -10.;

          // set to track speed and distance of the vehicle in front and adjust
          // speed accordingly
          double traffic_speed = speed_limit;
          double traffic_distance = too_close_meters_in_front;

          for (auto &car : sensor_fusion) {
            // car is in my lane
            double d = car[6];
            if (d < (2. + 4. * lane) + 2 && d > (2. + 4. * lane) - 2) {
              double vx = car[3];
              double vy = car[4];

              double check_speed = sqrt(
                  vx * vx + vy * vy); // magnitude of vector -- lane witdh = 4m
              double check_car_s = car[5];

              // project its s value outwards in time
              check_car_s +=
                  static_cast<double>(prev_path_size) * .02 * check_speed;

              // check that is greater than ours and if the gap is small
              double s_gap = check_car_s - ego_car_s;
              if (check_car_s > ego_car_s &&
                  s_gap < too_close_meters_in_front) {

                // we're too close to something in front of us
                too_close = true;
                if (traffic_speed > check_speed)
                  traffic_speed = check_speed;
                if (traffic_distance > s_gap)
                  traffic_distance = s_gap;
              }
            }
          }

          // slow down if too close
          double brake_speed =
              .0045 *
              speed_limit; // I'm speed racing to test, so this helps in slowing
                           // down faster if I set the car to move faster
          if (too_close && ref_speed > (traffic_speed - 2. * brake_speed)) {
            // slow speed of at least 1 meter per second, but proportionally to
            // the car in front's distance up to double that
            if (traffic_distance < 0.1)
              traffic_distance = 0.1; // to avoid numerical instabilities

            ref_speed -= brake_speed *
                         (1. + (1. - exp(1. - too_close_meters_in_front /
                                                      traffic_distance)));
          } else if (ref_speed < speed_limit) {
            ref_speed += 2*brake_speed;
          } else if (ref_speed > speed_limit) {
            ref_speed -= brake_speed;
          }

          // try to lane change if too close
          if (too_close) {
            // which lane can we move to?
            vector<int> possible_lane_changes;
            vector<tuple<int, double, double>>
                safe_lanes; // lane id, smallest positive gap and associated
                            // speed
            switch (lane) {
            case 0:
            case 2:
              possible_lane_changes = {1};
              break;
            case 1:
              possible_lane_changes = {0, 2};
              break;
            }

            bool lane_shift_safe;
            double min_s_gap = 999.;
            double speed_closest_car = speed_limit;
            for (auto const l : possible_lane_changes) {
              lane_shift_safe = true; // until proven wrong
              // check if there's a car in our proximity
              // in the lane we want to shift to
              double l_center = 2. + 4. * static_cast<double>(l);

              for (auto &car : sensor_fusion) {
                double d = car[6];
                if (d < l_center + 2. && d > l_center - 2.) {
                  // car is in the lane I want to shift to
                  double vx = car[3];
                  double vy = car[4];

                  double check_speed =
                      sqrt(vx * vx +
                           vy * vy); // magnitude of vector -- lane witdh = 4m
                  double check_car_s = car[5];

                  // project its s value outwards in time
                  check_car_s +=
                      static_cast<double>(prev_path_size) * .02 * check_speed;

                  // check that the other lane is safe to shift into
                  double s_gap = check_car_s - ego_car_s;
                  if (s_gap > too_close_meters_behind &&
                      s_gap < too_close_meters_in_front) {
                    lane_shift_safe = false;
                  } else {
                    if (s_gap > 0 && s_gap < min_s_gap) {
                      // track gap and speed of the closest car
                      min_s_gap = s_gap;
                      speed_closest_car = check_speed;
                    }
                  }
                }
              }
              if (lane_shift_safe) {
                safe_lanes.push_back(tuple<int, double, double>(
                    l, min_s_gap, speed_closest_car));
              }
            }

            // after having checked all lanes, decide the best one based on car
            // distances
            if (safe_lanes.size() > 0) {
              auto min_cost_idx = std::min_element(
                  safe_lanes.begin(), safe_lanes.end(),
                  [&too_close_meters_in_front,
                   &speed_limit](const tuple<int, double, double> &a,
                                 const tuple<int, double, double> &b) {
                    auto f = [&too_close_meters_in_front,
                              &speed_limit](double g, double v) {
                      return 0.5 * (too_close_meters_in_front /
                                    g) // farther is better
                             + 0.5 * (speed_limit - v) /
                                   speed_limit; // faster is better
                    };
                    return f(std::get<1>(a), std::get<2>(a)) <
                           f(std::get<1>(b), std::get<2>(b));
                  });
              lane = std::get<0>(*min_cost_idx);
            }
          }

          // reference x,y,yaw state
          double ref_x = ego_car_x;
          double ref_y = ego_car_y;
          double ref_yaw = deg2rad(ego_car_yaw); // reference in radias

          // coarse poits for the path planning
          vector<double> ptsx;
          vector<double> ptsy;

          // if previous path is almost empty
          if (prev_path_size < 2) {

            // use two points that make the path tangent to the car
            double prev_ego_car_x = ego_car_x - cos(ego_car_yaw);
            double prev_ego_car_y = ego_car_y - sin(ego_car_yaw);

            ptsx.push_back(prev_ego_car_x);
            ptsx.push_back(ego_car_x);

            ptsy.push_back(prev_ego_car_y);
            ptsy.push_back(ego_car_y);

          } else {

            // redefine ref state as the previous path's end point
            ref_x = previous_path_x[prev_path_size - 1];
            ref_y = previous_path_y[prev_path_size - 1];

            // set the previous yaw using the last two points
            double ref_prev_x = previous_path_x[prev_path_size - 2];
            double ref_prev_y = previous_path_y[prev_path_size - 2];
            ref_yaw = atan2(ref_y - ref_prev_y, ref_x - ref_prev_x);

            // use the two points to make the path tangent to the previous
            // endpoint
            ptsx.push_back(ref_prev_x);
            ptsx.push_back(ref_x);

            ptsy.push_back(ref_prev_y);
            ptsy.push_back(ref_y);
          }

          // Now add 30m-spaced points ahead of the starting reference
          double target_x = 30.;
          auto next_wp0 =
              getXY(ego_car_s + target_x, 2. + 4. * lane, map_waypoints_s,
                    map_waypoints_x, map_waypoints_y);
          auto next_wp1 =
              getXY(ego_car_s + 2 * target_x, 2. + 4. * lane, map_waypoints_s,
                    map_waypoints_x, map_waypoints_y);
          auto next_wp2 =
              getXY(ego_car_s + 3 * target_x, 2. + 4. * lane, map_waypoints_s,
                    map_waypoints_x, map_waypoints_y);

          ptsx.push_back(next_wp0[0]);
          ptsx.push_back(next_wp1[0]);
          ptsx.push_back(next_wp2[0]);

          ptsy.push_back(next_wp0[1]);
          ptsy.push_back(next_wp1[1]);
          ptsy.push_back(next_wp2[1]);

          // Shift the car reference to be (0,0,0) -- this makes spline compute
          // easier we go back to global later at (*)
          for (size_t i = 0; i < ptsx.size(); ++i) {
            double shift_x = ptsx[i] - ref_x;
            double shift_y = ptsy[i] - ref_y;

            ptsx[i] = shift_x * cos(0. - ref_yaw) - shift_y * sin(0. - ref_yaw);
            ptsy[i] = shift_x * sin(0. - ref_yaw) + shift_y * cos(0. - ref_yaw);
          }

          // Create a spline and set ptsx/y to it
          tk::spline s;
          s.set_points(ptsx, ptsy);

          // Use the spline to get the actual x and y points used for the
          // planner
          vector<double> next_path_x;
          vector<double> next_path_y;

          // push the remaining points from last path
          for (size_t i = 0; i < prev_path_size; ++i) {
            next_path_x.push_back(previous_path_x[i]);
            next_path_y.push_back(previous_path_y[i]);
          }

          // Then add the spline-generated ones
          double target_y = s(target_x);
          double target_dist = sqrt(target_x * target_x + target_y * target_y);
          double N =
              target_dist /
              (.02 * (ref_speed / 2.24)); // 0.02 -> interval seconds, 2.24 ->
                                          // from mph to meters per seconds

          double x_add_on = 0;

          for (size_t i = 0; i < planned_path_size - prev_path_size; i++) {

            // figure out next point
            double x_point = x_add_on + target_x / N;
            double y_point = s(x_point);

            // update x_add_on
            x_add_on = x_point;

            // rotate back to global coordinates (*)
            double x_ref = x_point;
            double y_ref = y_point;

            x_point = x_ref * cos(ref_yaw) - y_ref * sin(ref_yaw);
            x_point += ref_x;

            y_point = x_ref * sin(ref_yaw) + y_ref * cos(ref_yaw);
            y_point += ref_y;

            // finally push them back to the planner
            next_path_x.push_back(x_point);
            next_path_y.push_back(y_point);
          }

          // Push them to the JSON and send the message
          json msgJson;
          msgJson["next_x"] = next_path_x;
          msgJson["next_y"] = next_path_y;

          auto msg = "42[\"control\"," + msgJson.dump() + "]";

          ws.send(msg.data(), msg.length(), uWS::OpCode::TEXT);
        } // end "telemetry" if
      } else {
        // Manual driving
        std::string msg = "42[\"manual\",{}]";
        ws.send(msg.data(), msg.length(), uWS::OpCode::TEXT);
      }
    } // end websocket if
  }); // end h.onMessage

  h.onConnection([&h](uWS::WebSocket<uWS::SERVER> ws, uWS::HttpRequest req) {
    std::cout << "Connected!!!" << std::endl;
  });

  h.onDisconnection([&h](uWS::WebSocket<uWS::SERVER> ws, int code,
                         char *message, size_t length) {
    ws.close();
    std::cout << "Disconnected" << std::endl;
  });

  int port = 4567;
  if (h.listen(port)) {
    std::cout << "Listening to port " << port << std::endl;
  } else {
    std::cerr << "Failed to listen to port" << std::endl;
    return -1;
  }

  h.run();
}
