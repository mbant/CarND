# ----------
# User Instructions:
#
# Define a function, search() that returns a list
# in the form of [optimal path length, row, col]. For
# the grid shown below, your function should output
# [11, 4, 5].
#
# If there is no valid path from the start point
# to the goal, your function should return the string
# 'fail'
# ----------

# Grid format:
#   0 = Navigable space
#   1 = Occupied space


grid = [[0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0],
        [0, 0, 0, 0, 1, 0],
        [0, 0, 1, 1, 1, 0],
        [0, 0, 0, 0, 1, 0]]
init = [0, 0]
goal = [len(grid)-1, len(grid[0])-1]
cost = 1

delta = [[-1, 0],  # go up
         [0, -1],  # go left
         [1, 0],  # go down
         [0, 1]]  # go right

delta_name = ['^', '<', 'v', '>']


def search(grid, init, goal, cost):

    open = [{'point': init, 'g': 0}]
    closed_grid = [[0 for row in range(len(grid[0]))]
                   for col in range(len(grid))]
    found = False

    while len(open) > 0:

        # print(open)

        # find the point with min g in open
        g_values = [el["g"] for el in open]
        min_idx = g_values.index(min(g_values))

        current = open.pop(min_idx)
        current_x = current["point"][0]
        current_y = current["point"][1]
        current_g = current["g"]

        # print(f"analysing {current}")

        # if it's the goal, stop
        if [current_x, current_y] == goal:
            path = [current_g, current_x, current_y]
            found = True
            # print(f"goal found at {current}")
            break
        # otherwise expand it ...
        else:
            for i in range(len(delta)):
                new_point = [current_x + delta[i][0], current_y + delta[i][1]]
                # print(f"Trying to add {new_point}")
                if new_point[0] >= 0 and new_point[1] >= 0 and \
                        new_point[0] < len(grid) and new_point[1] < len(grid[0]):  # inside grid
                    if grid[new_point[0]][new_point[1]] == 0:  # walkable terrain
                        if closed_grid[new_point[0]][new_point[1]] == 0:  # still available
                            open.append(
                                {'point': new_point, 'g': current_g+cost})
            # print(open)

        # and close it
        closed_grid[current_x][current_y] = 1
        # print(closed_grid)

    if not found:
        path = 'fail'

    return path


print(search(grid, init, goal, cost))
