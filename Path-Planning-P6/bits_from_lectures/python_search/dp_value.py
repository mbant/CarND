# ----------
# User Instructions:
#
# Create a function compute_value which returns
# a grid of values. The value of a cell is the minimum
# number of moves required to get from the cell to the goal.
#
# If a cell is a wall or it is impossible to reach the goal from a cell,
# assign that cell a value of 99.
# ----------

grid = [[0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0]]
goal = [len(grid)-1, len(grid[0])-1]
cost = 1  # the cost associated with moving from a cell to an adjacent one

delta = [[-1, 0],  # go up
         [0, -1],  # go left
         [1, 0],  # go down
         [0, 1]]  # go right

delta_name = ['^', '<', 'v', '>']


def compute_value(grid, goal, cost):

    # set initially all the values to a high number (I'd do it negative, but the grader requires 99)
    value = [[99 for col in range(len(grid[0]))] for row in range(len(grid))]

    changed = True

    while changed:
        changed = False

        for x in range(len(grid)):
            for y in range(len(grid[0])):

                if [x, y] == goal:
                    if value[x][y] > 0:
                        value[x][y] = 0
                        changed = True

                elif grid[x][y] == 0:  # is walkable
                    for a in range(len(delta)):
                        x2, y2 = (x-delta[a][0], y-delta[a][1])

                        # inside grid and walkable terrain
                        if x2 >= 0 and y2 >= 0 and x2 < len(grid) and y2 < len(grid[0]) and grid[x2][y2] == 0:
                            # compute new possible value for x,y
                            possible_new_value = value[x2][y2] + cost

                            if possible_new_value < value[x][y]:
                                value[x][y] = possible_new_value
                                changed = True
        for v in value:
            print(v)
        print()

    return value


for v in compute_value(grid, goal, cost):
    print(v)
