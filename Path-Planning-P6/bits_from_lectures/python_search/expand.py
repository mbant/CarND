# -----------
# User Instructions:
#
# Modify the function search so that it returns
# a table of values called expand. This table
# will keep track of which step each node was
# expanded.
#
# Make sure that the initial cell in the grid
# you return has the value 0.
# ----------

# Grid format:
#   0 = Navigable space
#   1 = Occupied space


grid = [[0, 0, 1, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 1, 0, 1, 0],
        [0, 0, 1, 0, 1, 0],
        [0, 0, 1, 0, 1, 0]]

grader_grid = [[0, 1, 1, 1, 1],
               [0, 1, 0, 0, 0],
               [0, 0, 0, 1, 0],
               [1, 1, 1, 1, 0],
               [0, 0, 0, 1, 0]]

init = [0, 0]
goal = [len(grid)-1, len(grid[0])-1]
grader_goal = [len(grader_grid)-1, len(grader_grid[0])-1]
cost = 1

delta = [[-1, 0],  # go up
         [0, -1],  # go left
         [1, 0],  # go down
         [0, 1]]  # go right

delta_name = ['^', '<', 'v', '>']


def search(grid, init, goal, cost):

    open = [{'point': init, 'g': 0}]
    closed_grid = [[0 for col in range(len(grid[0]))]
                   for row in range(len(grid))]

    count = 0
    expand_grid = [[-1 for col in range(len(grid[0]))]
                   for row in range(len(grid))]

    found = False

    while len(open) > 0:

        # print(open)
        # find the point with min g in open
        g_values = [el["g"] for el in open]
        min_idx = g_values.index(min(g_values))

        current = open.pop(min_idx)
        x = current["point"][0]
        y = current["point"][1]
        g = current["g"]

        expand_grid[x][y] = count
        count += 1

        # print(f"analysing {current}")

        # if it's the goal, stop
        if [x, y] == goal:
            path = [g, x, y]
            found = True
            # print(f"goal found at {current}")
            break
        # otherwise close it and expand it ...
        else:
            closed_grid[x][y] = 1
            for i in range(len(delta)):
                new_point = [x + delta[i][0], y + delta[i][1]]
                # print(f"Trying to add {new_point}")
                if new_point[0] >= 0 and new_point[1] >= 0 and \
                        new_point[0] < len(grid) and new_point[1] < len(grid[0]):  # inside grid
                    if grid[new_point[0]][new_point[1]] == 0:  # walkable terrain
                        if closed_grid[new_point[0]][new_point[1]] == 0:  # still available
                            open.append(
                                {'point': new_point, 'g': g+cost})
        # print(open)
        # print(closed_grid)
        # print(expand_grid)
        # input(f"{found}")

    if not found:
        path = 'fail'

    return expand_grid


print(search(grid, init, goal, cost))
print(search(grader_grid, init, grader_goal, cost))
