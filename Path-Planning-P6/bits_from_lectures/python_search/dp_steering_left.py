# ----------
# User Instructions:
#
# Implement the function optimum_policy2D below.
#
# You are given a car in grid with initial state
# init. Your task is to compute and return the car's
# optimal path to the position specified in goal;
# the costs for each motion are as defined in cost.
#
# There are four motion directions: up, left, down, and right.
# Increasing the index in this array corresponds to making a
# a left turn, and decreasing the index corresponds to making a
# right turn.

forward = [
    [-1, 0],  # go up
    [0, -1],  # go left
    [1, 0],  # go down
    [0, 1]
]  # go right
forward_name = ['^', '<', 'v', '>']
# action has 3 values: right turn, no turn, left turn
action = [-1, 0, 1]
action_name = ['R', '#', 'L']

# EXAMPLE INPUTS:
# grid format:
#     0 = navigable space
#     1 = unnavigable space
grid = [[1, 1, 1, 0, 0, 0], [1, 1, 1, 0, 1, 0], [0, 0, 0, 0, 0, 0],
        [1, 1, 1, 0, 1, 1], [1, 1, 1, 0, 1, 1]]
init = [4, 3, 0]  # given in the form [row,col,direction]
# direction = 0: up
#             1: left
#             2: down
#             3: right

goal = [2, 0]  # given in the form [row,col]

# EXAMPLE OUTPUT:
# calling optimum_policy2D with the given parameters should return
# [[' ', ' ', ' ', 'R', '#', 'R'],
#  [' ', ' ', ' ', '#', ' ', '#'],
#  ['*', '#', '#', '#', '#', 'R'],
#  [' ', ' ', ' ', '#', ' ', ' '],
#  [' ', ' ', ' ', '#', ' ', ' ']]
# ----------

# ----------------------------------------
# modify code below
# ----------------------------------------


def optimum_policy2D(grid, init, goal, cost):
    value = [[[999 for col in range(len(grid[0]))] for row in range(len(grid))]
             for o in range(4)]
    policy = [[[' ' for col in range(len(grid[0]))] for row in range(len(grid))]
              for o in range(4)]
    policy2D = [[' ' for col in range(len(grid[0]))]
                for row in range(len(grid))]
    plan = [[[' ' for col in range(len(grid[0]))] for row in range(len(grid))]
            for o in range(4)]

    change = True
    while change:
        change = False
        for x in range(len(grid)):
            for y in range(len(grid[0])):
                for yaw in range(4):
                    if x == goal[0] and y == goal[1]:
                        if value[yaw][x][y] > 0:
                            change = True
                            value[yaw][x][y] = 0
                            policy[yaw][x][y] = '*'
                            plan[yaw][x][y] = '*'
                    elif grid[x][y] == 0:
                        for i in range(len(action)):
                            yaw2 = (yaw + action[i]) % 4
                            x2 = x + forward[yaw2][0]
                            y2 = y + forward[yaw2][1]
                            if 0 <= x2 < len(grid) and 0 <= y2 < len(
                                    grid[0]) and grid[x2][y2] == 0:
                                if value[yaw][x][y] > value[yaw2][x2][y2] + cost[i]:
                                    policy[yaw][x][y] = action_name[i]
                                    plan[yaw][x][y] = forward_name[i]
                                    change = True
                                    value[yaw][x][
                                        y] = value[yaw2][x2][y2] + cost[i]
    x, y, yaw = init
    policy2D[x][y] = policy[yaw][x][y]
    while policy[yaw][x][y] is not '*':
        yaw2 = (yaw + action[action_name.index(policy[yaw][x][y])]) % 4
        x += forward[yaw2][0]
        y += forward[yaw2][1]
        yaw = yaw2
        policy2D[x][y] = policy[yaw][x][y]
    return policy2D


cost = [2, 1, 20]  # cost has 3 values, corresponding to making
# a right turn, no turn, and a left turn
policy2D = optimum_policy2D(grid, init, goal, cost)

for p in policy2D:
    print(p)

print()
print()

cost = [2, 1, 3]
policy2D = optimum_policy2D(grid, init, goal, cost)

for p in policy2D:
    print(p)
