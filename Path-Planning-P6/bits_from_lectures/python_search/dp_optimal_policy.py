# ----------
# User Instructions:
#
# Write a function optimum_policy that returns
# a grid which shows the optimum policy for robot
# motion. This means there should be an optimum
# direction associated with each navigable cell from
# which the goal can be reached.
#
# Unnavigable cells as well as cells from which
# the goal cannot be reached should have a string
# containing a single space (' '), as shown in the
# previous video. The goal cell should have '*'.
# ----------

grid = [[0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0]]
goal = [len(grid)-1, len(grid[0])-1]
cost = 1  # the cost associated with moving from a cell to an adjacent one

delta = [[-1, 0],  # go up
         [0, -1],  # go left
         [1, 0],  # go down
         [0, 1]]  # go right

delta_name = ['^', '<', 'v', '>']


def compute_value_policy(grid, goal, cost):

    # set initially all the values to a high number (I'd do it negative, but the grader requires 99)
    value = [[99 for col in range(len(grid[0]))] for row in range(len(grid))]
    policy = [[' ' for col in range(len(grid[0]))] for row in range(len(grid))]

    changed = True

    while changed:
        changed = False

        for x in range(len(grid)):
            for y in range(len(grid[0])):

                if [x, y] == goal:
                    if value[x][y] > 0:
                        value[x][y] = 0
                        policy[x][y] = '*'
                        changed = True

                elif grid[x][y] == 0:  # is walkable
                    for a in range(len(delta)):
                        x2, y2 = (x+delta[a][0], y+delta[a][1])

                        # inside grid and walkable terrain
                        if x2 >= 0 and y2 >= 0 and x2 < len(grid) and y2 < len(grid[0]) and grid[x2][y2] == 0:
                            # compute new possible value for x,y
                            possible_new_value = value[x2][y2] + cost

                            if possible_new_value < value[x][y]:
                                value[x][y] = possible_new_value
                                policy[x][y] = delta_name[a]
                                changed = True

    return value, policy


value, policy = compute_value_policy(grid, goal, cost)
for p in policy:
    print(p)
