#ifndef CLASSIFIER_H
#define CLASSIFIER_H

#include <eigen3/Eigen/Dense> //#include "Dense"
#include <string>
#include <vector>

using Eigen::ArrayXd;
using std::string;
using std::vector;

class GNB {
public:
  /**
   * Constructor
   */
  GNB(double lw);

  /**
   * Destructor
   */
  virtual ~GNB();

  /**
   * Train classifier
   */
  void train(const vector<vector<double>> &data, const vector<string> &labels);

  /**
   * Predict with trained classifier
   */
  string predict(const vector<double> &sample);

private:
  double lane_width;
  vector<string> possible_labels = {"left", "keep", "right"};

  /*
    For each label/feature combination we'll have both the mean and the variance
    of the corresponding gaussian The outer vector has 3 elements, one for each
    label the inner one has one element for each feature, which are
        s               -> s coordinate
        d               -> d coordinate
        d % lane_width  -> relative position of the car in a lane
        s_dot           -> change in s
        d_dot           -> change in d
   */
  vector<double> label_prior;
  vector<vector<double>> mu;
  vector<vector<double>> sigma2;
};

#endif // CLASSIFIER_H