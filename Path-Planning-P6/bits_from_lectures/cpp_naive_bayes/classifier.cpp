#include <algorithm>
#include <iostream>
#include <math.h>
#include <string>
#include <vector>

#include "classifier.h"

using std::string;
using std::vector;

#define __DEBUG 0

// Initializes GNB
GNB::GNB(double lw) : lane_width(lw), label_prior(3, 0), mu(3), sigma2(3) {
  for (auto &m : mu) {
    m = vector<double>(5);
  }
  for (auto &s : sigma2) {
    s = vector<double>(5);
  }
}

GNB::~GNB() {}

void GNB::train(const vector<vector<double>> &data,
                const vector<string> &labels) {
  /**
   * Trains the classifier with N data points and labels.
   * @param data - array of N observations
   *   - Each observation is a tuple with 4 values: s, d, s_dot and d_dot.
   *   - Example : [[3.5, 0.1, 5.9, -0.02],
   *                [8.0, -0.3, 3.0, 2.2],
   *                 ...
   *                ]
   * @param labels - array of N labels
   *   - Each label is one of "left", "keep", or "right".
   */

  // For each label/feature combination, compute mu and sigma2
  size_t n_samples = data.size();
  for (size_t i = 0; i < n_samples; ++i) {

    int label{2};
    if (labels[i] == "left") {
      label = 0;
    } else if (labels[i] == "keep") {
      label = 1;
    }

    // count label occurrence
    label_prior[label] += 1.;

    // add values for the means
    mu[label][0] += data[i][0];
    mu[label][1] += data[i][1];
    mu[label][2] += fmod(data[i][1], lane_width);
    mu[label][3] += data[i][2];
    mu[label][4] += data[i][3];

    // add values for sigma2 (save sum of squares for now)
    sigma2[label][0] += data[i][0] * data[i][0];
    sigma2[label][1] += data[i][1] * data[i][1];
    sigma2[label][2] +=
        fmod(data[i][1], lane_width) * fmod(data[i][1], lane_width);
    sigma2[label][3] += data[i][2] * data[i][2];
    sigma2[label][4] += data[i][3] * data[i][3];

#if __DEBUG
    std::cout << "Label " << label + 1 << std::endl;
    std::cout << "Occurences till now " << label_prior[label] << std::endl
              << "Data : " << std::endl;
    for (auto &&d : data[i]) {
      std::cout << d;
    }
    std::cout << std::endl;
    std::cout << "Sum and sum of squares till now: " << std::endl;
    for (size_t i = 0; i < 4; i++) {
      std::cout << mu[label][i] << " " << sigma2[label][i] << std::endl;
    }
    std::cout << std::endl;

    int t;
    std::cin >> t;
#endif
  }

  for (size_t label = 0; label < 3; ++label) {
    if (label_prior[label] > 0) { // check for at least one occurrence
      for (size_t i = 0; i < mu[label].size(); ++i) {

        // divide each mu element by the label count to get the mean
        mu[label][i] /= label_prior[label];
        // divide each sigma2 element by the label count
        // and subtract the squared mean to get the variance
        sigma2[label][i] =
            sigma2[label][i] / label_prior[label] - mu[label][i] * mu[label][i];
      }
    }

    // divide each label count by the number of samples to get the label prior
    label_prior[label] /= static_cast<double>(n_samples);
  }
#if __DEBUG
  for (auto &&ml : mu) {
    for (auto &&m : ml) {
      std::cout << m;
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;
  std::cout << std::endl;
  for (auto &&sl : sigma2) {
    for (auto &&s : sl) {
      std::cout << s;
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;
  std::cout << std::endl;
  for (auto &&lp : label_prior) {
    std::cout << lp;
  }
  std::cout << std::endl;
  std::cout << std::endl;
  int t;
  std::cin >> t;
#endif
}

// helper gaussian function
double log_gaussian(double x, double mu, double sigma2) {
  return -0.5 * log(2 * M_PI * sigma2) - 0.5 * (x - mu) * (x - mu) / sigma2;
}

string GNB::predict(const vector<double> &sample) {
  /**
   * Once trained, this method is called and expected to return
   *   a predicted behavior for the given observation.
   * @param sample - a 4 tuple with s, d, s_dot, d_dot.
   *   - Example: [3.5, 0.1, 8.5, -0.2]
   * @output A label representing the best guess of the classifier. Can
   *   be one of "left", "keep" or "right".
   */

  // Transform sample into features
  size_t feat_size{mu[0].size()};
  vector<double> features(feat_size, 0.);
  features[0] = sample[0];
  features[1] = sample[1];
  features[2] = fmod(sample[1], lane_width);
  features[3] = sample[2];
  features[4] = sample[3];

  // Compute the conditional log-probab for each feature/label combination
  vector<vector<double>> cond_log_prob(3);
  for (size_t label = 0; label < 3; ++label) {
    cond_log_prob[label] = vector<double>(feat_size, 0.);
    for (size_t feat = 0; feat < feat_size; ++feat) {
      cond_log_prob[label][feat] =
          log_gaussian(features[feat], mu[label][feat], sigma2[label][feat]);
    }
  }

  // Compute the joint log probability for each label
  vector<double> joint_log_prob(3, 0.);

  for (size_t label = 0; label < 3; ++label) {
    for (size_t feat = 0; feat < feat_size; ++feat) {
      if (feat != 2) // && feat != 1 && feat != 2) // to exclude some features
                     // (the suggested one doesn't seem all too good...)
        joint_log_prob[label] += cond_log_prob[label][feat];
    }
    joint_log_prob[label] += log(label_prior[label]); // add prior
  }
  size_t argmax = std::distance(
      joint_log_prob.begin(),
      std::max_element(joint_log_prob.begin(), joint_log_prob.end()));

// Argmax to get the most probable label
#if __DEBUG
  for (auto &j : joint_log_prob)
    std::cout << j;
  std::cout << std::endl;
  std::cout << argmax << std::endl << std::endl;
  int t;
  std::cin >> t;
#endif

  return this->possible_labels[argmax];
}