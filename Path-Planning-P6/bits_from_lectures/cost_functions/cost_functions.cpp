double distance_from_goal_cost(int goal_lane, int intended_lane, int final_lane,
                               double distance_from_goal) {
  /**
   * Compute the cost associated with planning or effectuating a lane change;
   * the cost increases as the distance from the intended(and final) lane
   * increase from the goal lane and this effect is amplified the closer we are
   * to the goal, encouraging lane changes only if benefiacial and sufficiently
   * far from the goal
   *
   * @param goal_lane - the lane associated with the goal
   * @param intended_lane - the intended lane for the given behaviour
   *    for "lane change" states and "planning lane change" states this is
   *    one lane left/right from the current one
   * @param final_lane - the IMMEDIATE resulting lane for the given behaviour
   *    this is one lane left/right from the current one only for "lane change"
   * states
   * @param distance_from_goal - delta_s, i.e. the distance from the goal
   */

  int delta_d = 2.0 * goal_lane - intended_lane - final_lane;
  return 1.0 - exp(-(std::abs(delta_d) / distance_from_goal));
}

double inefficiency_cost(int target_speed, int intended_lane, int final_lane,
                         const vector<int> &lane_speeds) {
  /**
   * Compute the cost associated with driving in an inefficient lane;
   * THe cost decreases as intended_lane and final_lane have speeds closer to
   * the target speed. Note that we don't take care of speed limits here.
   *
   * This should suggest the vehicle to drive in the fastest possible lane.
   *
   * @param target_speed - the intended speed of the vehicle
   * @param intended_lane - the intended lane for the given behaviour
   *    for "lane change" states and "planning lane change" states this is
   *    one lane left/right from the current one
   * @param final_lane - the IMMEDIATE resulting lane for the given behaviour
   *    this is one lane left/right from the current one only for "lane change"
   * @param lane_speeds - a vector of lane speeds, based on traffic in all lanes
   */

  double speed_intended = lane_speeds[intended_lane];
  double speed_final = lane_speeds[final_lane];
  return std::abs(2.0 * target_speed - speed_intended - speed_final) /
         target_speed;
}

// Many more could exist, like ..

double enforcing_speed_limits() {
  // cost to 1 if we're breaking speed limit
  return 0;
}

double safety_speed() {
  /*
   * used to encourage driving at a similar speed than surrounding traffic, even
   * if slightly inefficient, for safety reasons
   */
  return 0.;
}