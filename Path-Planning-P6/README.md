# CarND-Path-Planning-Project
Self-Driving Car Engineer Nanodegree Program

<p  style="text-align:center"><img src="lane_shift_smaller.gif" alt="drawing" width="400"/></p>

### Brief Description
In this project the simulated car safely navigate around a virtual highway with other traffic that is driving +-10 MPH of the 50 MPH speed limit. 
The car's localization and sensor fusion data are provided and there is also a sparse map list of waypoints around the highway. The car tries to go as close as possible to the 50 MPH speed limit, which means passing slower traffic when possible, and avoids hitting other cars as well as driving inside of the marked road lanes at all times, unless going from one lane to another. The path planned for the car do not exceed a total acceleration of 10 m/s^2 and jerk that is greater than 10 m/s^3, to simulate a comfortable drive.

### Technical details

All the code is in `main.cpp`.

The car starts accelerating from a complete stop, targeting a speed just below the speed limit ($49$ mph). It plans a path as close as possible to the center of the lane (making use of Frenet coordinates and position of the waypoints on the map to simplify the problem of tracking the lane).
As soon as another vehicle in front is detected to be closer than $30m$ (lines `116` to `173`), the car starts to slow down (slowing proportionally more as the  vehicle in front gets closer and closer, lines `160` to `170`) until it reaches a speed close to the one of the preceding vehicle; this helps in not slowing down too much, avoiding cycles of speeding up and slowing down while we wait to change lane.
At the same time it evaluates the possibility of a safe lane switch looking at the adjacent lane(s) and the vehicles present around itself. If it safe to switch (lines `176` to `257`) it does so, by prioritizing empty lanes first and then lanes with faster cars and farther away (this is achieved via a cost function, see lines from `237` to `257`); this allows for lane changes that improve its forward progress, avoiding `blind` changes that lead to an overall slowdown.
To avoid collisions at all costs we check of course both in front and behind the ego vehicle for close-by cars that could make theshift unsafe (see `217-221`).

All paths are planned (see from `260` to `378`) by keeping track of the previously followed path (lines `268` to `299`) and appending to them the wanted behaviours mainly via $3$ evenly spaced ($30m$ apart) points (`301` to `311`) at the end, using interpolating splines to smooth out the trajectory and avoiding excessive jerk or sudden acceleration. I'm using as suggested [this single header spline library](https://kluge.in-chemnitz.de/opensource/spline/) to do that, from line `331` to `387`.

For additional details, mostly useful to developers wating to improve on this, see `extra_details.md`

### Possible Improvements
1. For ease of reference while under review, I've left all the code in `main.cpp` but refactoring the code will become a necessity as I improve it; I've planned classes for the ego vehicle and other cars detected in order to separate the logic a bit better between different methods;
2. More cost functions associated with lane changing, slowing down and behviour in general are needed as in `bits_from_lectures/cost_functions` and `bits_from_lectures/behavioural_planner`;
3. For now I'm using spline interpolation to smooth out trajectories, but a better way would be to use jerk-minimising polynomials to get the optimal trajectory as in `bits_from_lectures/cpp_quintic_poly_solver`;
4. Finally, path planning can be greatly improved by using a more sophisticated planner that takes into account uncertainty, like a sampling method for example, rather than simply designing one deterministic trajectory. Points 2 and 3 will need to be addressed further in this case, adding a measure of uncertainty to any decided behaviour (like lane changing), and from there sampling multiple (rather than one) endpoints for the path and estimating jerk-minimising trajectories for each one.

## Basic Build Instructions

1. Clone this repo.
2. Make a build directory: `mkdir build && cd build`
3. Compile: `cmake .. && make`
4. Run it: `./path_planning`.

Take a look at dependencies at the bottom of this `README` as well.
You'll also need the simulator below to see the car in action.

### Simulator
You can download the Term3 Simulator which contains the Path Planning Project from the [releases tab (https://github.com/udacity/self-driving-car-sim/releases/tag/T3_v1.2).  

To run the simulator on Mac/Linux, first make the binary file executable with the following command:
```shell
sudo chmod u+x {simulator_file_name}
```


## Dependencies

* cmake >= 3.5
  * All OSes: [click here for installation instructions](https://cmake.org/install/)
* make >= 4.1
  * Linux: make is installed by default on most Linux distros
  * Mac: [install Xcode command line tools to get make](https://developer.apple.com/xcode/features/)
  * Windows: [Click here for installation instructions](http://gnuwin32.sourceforge.net/packages/make.htm)
* gcc/g++ >= 5.4
  * Linux: gcc / g++ is installed by default on most Linux distros
  * Mac: same deal as make - [install Xcode command line tools]((https://developer.apple.com/xcode/features/)
  * Windows: recommend using [MinGW](http://www.mingw.org/)
* [uWebSockets](https://github.com/uWebSockets/uWebSockets)
  * Run either `install-mac.sh` or `install-ubuntu.sh`.
  * If you install from source, checkout to commit `e94b6e1`, i.e.
    ```
    git clone https://github.com/uWebSockets/uWebSockets 
    cd uWebSockets
    git checkout e94b6e1
    ```