# **Finding Lane Lines on the Road** 
[![Udacity - Self-Driving Car NanoDegree](https://s3.amazonaws.com/udacity-sdc/github/shield-carnd.svg)](http://www.udacity.com/drive)

<img src="examples/laneLines_thirdPass.jpg" width="480" alt="Combined Image" />

Overview
---

When we drive, we use our eyes to decide where to go.  The lines on the road that show us where the lanes are act as our constant reference for where to steer the vehicle.  Naturally, one of the first things we would like to do in developing a self-driving car is to automatically detect lane lines using an algorithm.

In this project I use Python and OpenCV to detect lane lines by taking advantage of a pipeline build using the Canny algorithm to detect edges and Hough transform to detect lines amongst those edges. I then implemented a filter that averages the segments found into the two expected lane lines.

The Project is intended for use via docker using the [CarND Term1 Starter Kit](https://github.com/udacity/CarND-Term1-Starter-Kit/blob/master/README.md) and `jupyter notebooks`.

# **Finding Lane Lines on the Road** 

## 1. The Pipeline. 

My pipeline consists of 5 steps; I'll refer to the helper functions by using their names in my submission

1. Convert the images to grayscale using `grayscale()`;
2. Apply a Gaussian filter, smoothing the image using `gaussian_blur()` with a `kernel_size = 5` ;
3. Apply the Canny algorithm to detect edges in the image using `canny()` and restrict the results only to a given `region_of_interest()`;
4. Apply the Hough transform and though that identify segments in the image by applying `hough_lines()`;
5. Filter the lanes found to produce two lines representing left and right lanes using `find_left_and_right_lines()`;
6. Draw the two lanes on the original image using `draw_lines()` and `weighted_image()`

The `region_of_interest()` is shown below in blue and defined by a trapezoid with `vertices`

    0.1*imshape[1],0.92*imshape[0])
    (imshape[1]*0.45, imshape[0]*0.6)
    (imshape[1]*0.55, imshape[0]*0.6)
    (imshape[1]*0.96,0.92*imshape[0])

![roi](roi.png)

Note that the base extends more toward the right side of the image, to correct for the slightly asymmetrical pose, and we don't extend all the way to the bottom as (for example in the challenge) small artifacts due to shadow from the car or the car trunk itself can show on the bottom of the image adding noise to the line detection.

Note also that I separated in smaller chunks the functionality of he original `hough_lines` which now takes the canny imageas an input but
output the segment list only, rather than drawing them on a new image.

Using this new structure I'm able to apply steps 3,4 and 5 iteratively, adapting the parameters. In particular I start by applying the Canny algorithm
using a quite restrictive `low_threshold = 85` and `high_threshold = 210` and `hough_lines()` with `rho = 2`, `theta = np.pi/180`, `threshold = 15` and in partiular `min_line_length = 40` and `max_line_gap = 20`.

`find_left_and_right_lines()` is run next and tries to identify the two road marks (explained later). 
If less than two lines are found we go back to step 4 and increment `max_line_gap` to `30`.
If this doesn't solve the problem we go back all the way to step 3 and repeat the whole detection part, 
starting from `canny()` but multiplying the starting `low_threshold` and `high_threshold` by `0.75` ... and so on until two lines are found or the `low_threshold` falls below 20 (which means 5 iterations).

Essentially we make both `canny()` and `hough_lines()` less and less strict until a proper result is found. This makes so that if the lane is easily detected
we won't have to deal with a lot of 'random' segment found as well by the pipeline with non-stringent parameters; if instead the lines are not clearly detected, we increase the sensitivity of steps 3 and 4 till something is found.

### `find_left_and_right_lines()`

This is the main improvement on the starting code we were provided (other than the iterative procedure described above).

We use a couple of important concept in detecting the lane lines:

* We expect to find two lines, with *circa* opposite slopes;
* the lines we expect should start and end in our region of interest;
* especially for non-continous lane lines we will detect segments of the line of interest, so it's important to average similar segments into a single line.

With that in mind we proceed to define that:

* With respect to the X axis, we want our detected lines to have a slope with absolute value between $25°$ and $60°$, which translate in slopes between `low_slope = math.tan( 25 * math.pi / 180 )` and `high_slope = math.tan( 60 * math.pi / 180 )`;
* the segments, extended to the region of interest (define by top two `vertices` and the two bottom corner of the image ) should start and end in the region;
* longer segment should weight more when averaging since it's likely that shorter segments come from noise, while longer are correctly detected parts of the lane lines.

For every line found by `hough_lines()` we compute its slope ( computed as `s  = (y2-y1)/(x2-x1)`), its bias `b = y1 - s*x1` and its start and end `x`-coordinate in the region of interest by 

    y_min = round( vertices[0][1][1] ) # this is the distance of the region of interest from the top of the image
    x_s = (imgshape[0]-b)/s # implied by x=(y_0-b)/slope with y_0=imgshape[0]
    x_e = (y_min-b)/s # X end implied by x=(y_min-b)/slope 

and then check these quantities on the three requirements above. If met, we compute the length of the segment by `l = math.sqrt( (x1-x2)**2 + (y1-y2)**2 )`
and store it, depending of its slope, in a list dediceted to either the right or the left line.

After scanning through all the segments, assuming we found at least one segment for both right and left lines, we average them (in particular their start and end points) proportionally to their original length into forming the two lane lines.

These lines are then passed on step 6 of the pipline.

## 2. Potential shortcomings and possible improvements to your pipeline

A first shortcoming of this pipeline is that it only fits lines, while we should really aim for slight curves!
While lines are a good local approximation on a straight and flat road, either a change in slope of the road (both uphill and downhill) or, even more obvious, a curve in the road makes so that the lane lines depart from being actual lines. This is evident in the jittering especially on the `challenge.mp4` video.

Of course in the same video another shortcoing is apparent; as the road changes from dark asphalt to a lighter color, the contrast with the yellow line on the left dimish and the current algorithm struggle to pick up the line. This is made even more complicated by some dark marks on the road that the pipeline sometime misinterpret for relevant segments. One thing that wasn't explored in my current project is the fact that we expect the lines to be either white or yellow (in EU we might also have some blue lines that delimit parkings, not sure about the US..); considering this while searching for the lanes might help in filtering out some unwanted segments.

Another improvement that would surely help is to use the informations from the past frame(s) to inform the current detection. We expect that the lane lines won't change drastically from one frame to the next so averaging the current detections with the identified lanes from the past frame might help alleviate some of the problems with the changing asphalt in the `challenge.mp4` video or, in general, frames where we might struggle to find one or more of the lane lines.

Connected with this, there's an obvious issue with my iterative procedure. This is fine on off-line videos but it might end up being way too slow on online video feeds from an acual self-driving car. A way to either buffer frames or carry over parameters from the previous frame might be needed.

Finally, a weighted region of interest might help as well, where rather than keeping every point inside and discarding what's outside, we might up-weight regions where we expect the lines and down-weight segments as they go farther from there (see the image below, where intensity of the blue color is proportional to weight).
[this is similar in concept to putting a 'Bayesian prior' on the region where we expect the lines]

![wroi](wroi.png)
